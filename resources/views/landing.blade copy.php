<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>QuickSwab - Swab Antigen Drive Thru</title>
  <meta content="QuickSwab, Antigen swab test drive thru
  Tidak perlu turun tidak perlu tunggu
  15mnt hasilnya dikirim langsung via email dan whatsapp" name="description">
  <meta content="quickswab, covid-19, corona, drivethru, drive thru, drive through, antigen, swab test, test, swab, pcr, sars-cov19" name="keywords">
  <meta property="og:title" content="QuickSwab - Swab Antigen Drive Thru">
    <meta property="og:description" content="QuickSwab, Antigen swab test drive thru
    Tidak perlu turun tidak perlu tunggu
    15mnt hasilnya dikirim langsung via email dan whatsapp">
    <meta property="og:image" content="https://quickswab.id/assets/images/logo-quickswab.png">
    <meta property="og:url" content="https://quickswab.id">
    <meta name="twitter:card" content="summary_large_image">


<!--  Non-Essential, But Recommended -->

<meta property="og:site_name" content="QuickSwab.id">
<meta name="twitter:image:alt" content="Quickswab">

  <!-- Favicons -->
  <link href="{{asset('assets/landing/assets/img/quick.png')}}" rel="icon">
  <link href="{{asset('assets/landing/assets/img/quick.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('assets/landing/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/landing/assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/landing/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/landing/assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
  <link href="{{asset('assets/landing/assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/landing/assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{asset('assets/landing/assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/landing/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('assets/landing/assets/css/style.css')}}" rel="stylesheet">

  <style>
    body, html {
    height: 100%;
}

/* The hero image */
.hero-image {
  /* Use "linear-gradient" to add a darken background effect to the image (photographer.jpg). This will make the text easier to read */
  background-image:  url("{{asset('assets/landing/assets/img/bannerlanding.png')}}");

  /* Set a specific height */
  height: 50%;

  /* Position and center the image to scale nicely on all screens */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  margin-top: 60px;
  margin-bottom: -100px;
}

/* Place text in the middle of the image */
.hero-text {
  text-align: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: white;
}
.google-maps {
        position: relative;
        padding-bottom: 75%; // This is the aspect ratio
        height: 0;
        overflow: hidden;
    }
    .google-maps iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
    }
  </style>

  <!-- =======================================================
  * Template Name: Medilab - v2.1.0
  * Template URL: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <!-- <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i> <a href="mailto:contact@example.com">contact@example.com</a>
        <i class="icofont-phone"></i> +1 5589 55488 55
        <i class="icofont-google-map"></i> A108 Adam Street, NY
      </div>
      <div class="social-links">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a>
      </div>
    </div>
  </div> -->

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <!-- <h1 class="logo mr-auto"><a href="index.html">Medilab</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="#" class="logo mr-auto"><img src="assets/landing/assets/img/logo-quickswab.png" alt="" class="img-fluid"></a>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="#departments">Lokasi</a></li>
          <li><a href="#contact">Kontak</a></li>

        </ul>
      </nav>

    </div>
  </header><!-- End Header -->

  <div class="hero-image">
    <div class="hero-text">
    </div>
  </div>

  <main id="main">

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container">

        <div class="row">
          <div class="col-lg-4 d-flex align-items-stretch">
            <div class="content">
              <h3>Kenapa Harus di QuickSwab?</h3>
              <p>
                Tidak Perlu Turun Tidak Perlu Nunggu. Hasilnya dikirim langsung ke Hp anda melalui e-mail Hanya 15 menit.
              </p>
            </div>
          </div>
          <div class="col-lg-8 d-flex align-items-stretch">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="ri-smartphone-line"></i>
                    <h4>Scan Barcode dan Daftar Lewat Smartphone</h4>
                    <p>Cukup scan qr code dan isi data diri lewat smartphone anda.</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="ri-car-line"></i>
                    <h4>Tidak Perlu Turun</h4>
                    <p>Kami memakai sistem drive thru jadi anda tidak perlu turun untuk Swab Antigen</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="ri-mail-line"></i>
                    <h4>Hasil Langsung di kirim</h4>
                    <p>Sudah selesai swab hasil akan dikirim melalui email dan whatsapp anda.</p>
                  </div>
                </div>
              </div>
            </div><!-- End .content-->
          </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Departments Section ======= -->
    <section id="departments" class="departments">
      <div class="container">

        <div class="section-title">
          <h2>Lokasi</h2>
        </div>

        <div>
          <div class="card shadow">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6">
                  <h5 class="card-title">Lokasi 1</h5>
                  <h5>Gedung The Historich</h5>
                  <h5>Jl. Gatot Subroto No.19</h5>
                  <h5>Baros - Cimahi</h5>
                  <h5>Senin-Minggu</h5>
                  <h5>Mulai dari pukul 08.00-17.00 WIB</h5>
                  <br>
                  <h5>Home service:</h5>
                  <h5>Minimal 2 orang</h5>
                  <br>
                  <h5>Hotline: 081121112267/022-7351327</h5>
                  <br>
                  <h5>1300090962021 Rek Bank Mandiri an. Alnajah Jasa</h5>
                  <h5></h5>
                </div>
                <div class="col-lg-6">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.0370166595494!2d107.535334315375!3d-6.886169669298176!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e4526befdd71%3A0x5ce9b3d8c76672c6!2sThe%20Historich%20Building!5e0!3m2!1sen!2sid!4v1611591155016!5m2!1sen!2sid" width="300" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
              </div>

              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Departments Section -->




    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2>Kontak Kami</h2>
        </div>
      </div>

      <div class="container">
        <div class="row mt-5">

          <div class="col-lg-12">
            <div class="info">
              <div class="row">
                <div class="col-md-4">
                  <div class="address">
                    <i class="icofont-google-map"></i>
                    <h4>Lokasi:</h4>
                    <p>Gedung The Historich</p>
                    <p>Jl. Gatot Subroto No.19</p>
                  </div>
                </div>

                <div class="col-md-4 mt-2">
                  <div class="email">
                    <i class="icofont-envelope"></i>
                    <h4>Email:</h4>
                    <p>quickswab@outlook.com</p>
                  </div>
                </div>
                <div class="col-md-4 mt-2">
                  <div class="phone">
                    <i class="icofont-phone"></i>
                    <h4>Call:</h4>
                    <p>081121112267</p>
                    <p>022-7351327</p>
                  </div>

                </div>
              </div>

            </div>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>QuickSwab</span></strong>. All Rights Reserved
        </div>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('assets/landing/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('assets/landing/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/landing/assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('assets/landing/assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{asset('assets/landing/assets/vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{asset('assets/landing/assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('assets/landing/assets/vendor/counterup/counterup.min.js')}}"></script>
  <script src="{{asset('assets/landing/assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('assets/landing/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('assets/landing/assets/js/main.js')}}"></script>

</body>

</html>
