<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>FastSwab - Layanan Swab Antigen & PCR</title>
      <link rel="shortcut icon" type="image/jpg" href="{{ asset('assets/images/logofs.png') }}"/>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
      <link rel="stylesheet" href="{{ asset('css/landing.css') }}">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <meta content="FastSwab, Layanan Swab Antigen & PCR, Layanan Swab Test Aman dan Terpercaya" name="description">
      <meta content="fastswab, covid-19, corona, drivethru, drive thru, drive through, antigen, swab test, test, swab, pcr, sars-cov19" name="keywords">
      <meta property="og:title" content="FastSwab - Layanan Swab Antigen & PCR">
      <meta property="og:description" content="FastSwab - Layanan Swab Antigen & PCR, Layanan Swab Test Aman dan Terpercaya">
      <meta property="og:image" content="https://fastswab.id/assets/images/fastswab.png">
      <meta property="og:url" content="https://fastswab.id">
      <meta name="twitter:card" content="summary_large_image">
      <meta name="robots" content="index, follow" />
      <link rel="canonical" href="http://fastswab.id/" />
      <meta property="og:site_name" content="FastSwab.id">
      <meta name="twitter:image:alt" content="FastSwab">
      <style>
          .map-responsive{

    overflow:hidden;

    padding-bottom:56.25%;

    position:relative;

    height:0;

}

.map-responsive iframe{

    left:0;

    top:0;

    height:100%;

    width:100%;

    position:absolute;

}
      </style>
    </head>
    <body>
       <section class="h-100 w-100 bg-white" style="box-sizing: border-box">
    <div class="header-4-2 container-xxl mx-auto p-0 position-relative" style="font-family: 'Poppins', sans-serif">
      <nav class="navbar navbar-expand-lg navbar-light">
        <a href="#">
          <img style="margin-right: 0.75rem"
            src="{{ asset('assets/images/fastswab.png') }}" width="150px" />
        </a>
        <button class="navbar-toggler border-0" type="button" data-bs-toggle="modal" data-bs-target="#targetModal-item">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="modal-item modal fade" id="targetModal-item" tabindex="-1" role="dialog"
          aria-labelledby="targetModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content bg-white border-0">
              <div class="modal-header border-0" style="padding: 2rem; padding-bottom: 0">
                <a class="modal-title" id="targetModalLabel">
                  <img style="margin-top: 0.5rem"
                    src="{{ asset('assets/images/fastswab.png') }}"
                    alt="FastSwab" width="150px" />
                </a>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body" style="padding: 2rem; padding-top: 0; padding-bottom: 0">
                <ul class="navbar-nav responsive me-auto mt-2 mt-lg-0">
                  <li class="nav-item">
                    <a class="nav-link" href="#home">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#service">Layanan</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#harga">Harga</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#lokasi">Lokasi</a>
                  </li>
                </ul>
              </div>
              <div class="modal-footer border-0 gap-3" style="padding: 2rem; padding-top: 0.75rem">
                <a href="#harga" class="btn btn-fill text-white">Daftar Swab</a>
              </div>
            </div>
          </div>
        </div>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo">
          <ul class="navbar-nav me-auto mt-2 mt-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="#home">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#service">Layanan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#harga">Harga</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#lokasi">Lokasi</a>
            </li>
          </ul>
          <div class="gap-3">
            <a href="#harga" class="btn btn-fill text-white">Daftar Swab Test</a>
          </div>
        </div>
      </nav>

      <div id="home">
        <div class="mx-auto d-flex flex-lg-row flex-column hero">
          <!-- Left Column -->
          <div
            class="left-column d-flex flex-lg-grow-1 flex-column align-items-lg-start text-lg-start align-items-center text-center">
            <h1 class="title-text-big">
              Layanan Swab Test Aman dan Terpercaya<br class="d-lg-block d-none" />
            </h1>
            {{-- <img src="{{ asset('assets/images/logosamita.png') }}" alt="" width="150px" class="img-fluid"> --}}
          </div>
          <!-- Right Column -->
          <div class="right-column text-center d-flex justify-content-lg-end justify-content-center pe-0">
            <img id="img-fluid" class="h-auto mw-100"
              src="{{ asset('assets/images/fastswabhead.png') }}"
              alt="FastSwab Bandung" width="600px" />
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="h-100 w-100 bg-white" style="box-sizing: border-box" id="service">
    <div class="content-2-2 container-xxl mx-auto p-0  position-relative" style="font-family: 'Poppins', sans-serif">
      <div class="text-center title-text">
        <h1 class="text-title" style="color: #0f3a53;">Layanan FastSwab</h1>
        <p class="text-caption" style="margin-left: 3rem; margin-right: 3rem; color: #0e709d;">
          Dapatkan layanan swab test aman, cepat dan terpercaya
        </p>
      </div>

      <div class="grid-padding text-center">
        <div class="row">
          <div class="col-lg-4 column">
            <div class="icon">
              <i class="fa fa-clock-o fa-lg" aria-hidden="true" style="color: #0e709d"></i>
            </div>
            <h3 class="icon-title" style="color: #0f3a53;">Proses Cepat</h3>
            <p class="icon-caption" style="color: #0e709d;">
              Proses Keseluruhan Hanya<br />
              membutuhkan 10 menit.
            </p>
          </div>
          <div class="col-lg-4 column">
            <div class="icon">
              <i class="fa fa-mobile fa-lg" aria-hidden="true" style="color: #0e709d"></i>
            </div>
            <h3 class="icon-title" style="color: #0f3a53;">Hasil Digital</h3>
            <p class="icon-caption" style="color: #0e709d;">
              Selain mendapatkan hasil fisik<br />
              Anda akan mendapatkan hasil  <br>
              digital dikirim melalui E-mail atau Whatsapp
            </p>
          </div>
          <div class="col-lg-4 column">
            <div class="icon">
              <i class="fa fa-users fa-lg" aria-hidden="true" style="color: #0e709d"></i>
            </div>
            <h3 class="icon-title" style="color: #0f3a53;">Profesional</h3>
            <p class="icon-caption" style="color: #0e709d;">
              Petugas layanan kami terlatih <br>
              dan siap melayani Anda.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="harga" class="h-100 w-100" style="box-sizing: border-box; background-color: #f2f6ff">
    <div class="content-3-7 overflow-hidden container-xxl mx-auto position-relative"
      style="font-family: 'Poppins', sans-serif">
      <div class="container mx-auto">
        <div class="d-flex flex-column text-center w-100" style="margin-bottom: 2.25rem">
          <h2 class="title-text">Daftar Layanan Swab Test</h2>
        </div>
        <div class="d-flex flex-wrap">
          <div class="mx-auto card-item position-relative">
            <div class="card-item-outline bg-white d-flex flex-column position-relative overflow-hidden h-100">
              <h2 class="price-title">Swab Antigen</h2>
              <h2 class="price-value d-flex align-items-center">
                <span>Rp.99.000</span>
                <!-- <span class="price-duration">/Month</span> -->
              </h2>
              <p class="price-caption">
                Hanya 10 Menit
              </p>
              <a href="https://api.whatsapp.com/send?phone=6281234502205" target="_blank" class="btn btn-outline d-flex justify-content-center align-items-center w-100">
                Info
              </a>
            </div>
          </div>
          <div class="mx-auto card-item position-relative">
            <div class="card-item-outline bg-white d-flex flex-column position-relative overflow-hidden h-100">
              <h2 class="price-title">Swab PCR</h2>
              <h2 class="price-value d-flex align-items-center">
                <span>Rp.275.000</span>
                <!-- <span class="price-duration">/Month</span> -->
              </h2>
              <p class="price-caption">
                Hasil H+1
              </p>
              <a href="https://api.whatsapp.com/send?phone=6281234502205" target="_blank" class="btn btn-outline d-flex justify-content-center align-items-center w-100">
                Info
              </a>
            </div>
          </div>
          <div class="mx-auto card-item position-relative">
            <div class="card-item-outline d-flex flex-column position-relative overflow-hidden h-100"
              style="background-color: #2e3a53">
              <h2 class="price-title text-white">Home Service</h2>
              <h2 class="price-value d-flex align-items-center text-white">
                <span>Rp.125.000</span>
              </h2>
              <p class="price-caption" style="color: #7987a6">
                /Kunjungan<br />
              </p>
              <a href="https://api.whatsapp.com/send?phone=6281234502205" target="_blank" class="btn btn-fill text-white d-flex justify-content-center align-items-center w-100">
                Info
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="h-100 w-100 bg-white" style="box-sizing: border-box" id="lokasi">
    <div class="content-2-2 container-xxl mx-auto p-0  position-relative" style="font-family: 'Poppins', sans-serif">
      <div class="text-center title-text">
        <h1 class="text-title" style="color: #0f3a53;">Lokasi Kami</h1>
        <p class="text-caption text-center" style="margin-left: 3rem; margin-right: 3rem; color: #0e709d;">
          Datang ke lokasi kami untuk mendapatkan layanan Drive Thru Swab Test
        </p>
      </div>


      <div class="grid-padding">
        <div class="row">
          <div class="col-lg-6 column">
            <p class="icon-caption" style="color: #0e709d;">
              Jl. Jakarta No 18, Kacapiring, Kec.Batununggal, Bandung 40271 (GOR KONI BANDUNG)
            </p>
            <p class="icon-caption" style="color: #0e709d;">
              LABORATORIUM KLINIK PRATAMA MIKROTEST<br>
              JI Raya Soreang No. 9 RT 01 RW 04<br>
              Pamekaran - Kec. Sorean	<br>
              Kab. Bandung - Jawa Barat<br>
            </p>
            <p class="icon-caption" style="color: #0e709d;">
              08 12345 022 05 <br>
              Email: fastswab@gmail.com
            </p>
          </div>
          <div class="col-lg-6 column">
              <div class="map-responsive">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.7933550553107!2d107.63404171535717!3d-6.91529256960513!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e7c5032605ff%3A0x76a6bdc7672f2ab5!2sGOR%20KONI%20-%20Kota%20Bandung!5e0!3m2!1sid!2sid!4v1635950780376!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="h-100 w-100 bg-white" style="box-sizing: border-box">

		<div class="footer-2-2 container-xxl mx-auto position-relative p-0" style="font-family: 'Poppins', sans-serif">


			<div class="border-color info-footer">
				<div class="">
					<hr class="hr" />
				</div>
				<div class="mx-auto d-flex flex-column flex-lg-row align-items-center footer-info-space gap-4">
					<nav class="d-flex flex-lg-row flex-column align-items-center justify-content-center">
						<p style="margin: 0">Copyright © 2021 FastSwab</p>
					</nav>
				</div>
			</div>
		</div>
	</section>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    </body>
  </html>
