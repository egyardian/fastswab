<div class="card">
    <div class="card-body">

        <div class="table-responsive">
            <table class="table table-bordered" >
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Jumlah Pasien</th>
                        <th>Test</th>
                        <th>Note</th>
                        <th>Tanggal</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transaction as $item)
                    <tr>
                        <td>
                            {{ $item->code }}
                        </td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->phone }}</td>
                        <td>{{ count($item->detail) }}</td>
                        <td>{{ $item->detail->first()->name_test }}</td>
                        <td>{{ $item->note }}</td>
                        <td>{{ $item->updated_at }}</td>
                        <td>Rp. {{ number_format($item->amount) }}</td>
                    </tr>

                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="8">GRAND TOTAL</th>
                        <th>Rp. {{ number_format($transaction->sum('amount')) }}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
