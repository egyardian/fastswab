@extends('layouts.index')
@section('title','Swab - Input Hasil')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
            <h2 class="hk-pg-title font-weight-600 mb-10">Swab - Input Hasil</h2>
            <small>{{$transactions->code}}</small>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            <div class="mb-2">
                <a href="{{route('dokter.datapasien',$lab->id)}}" class="btn btn-primary">Kembali</a>
            </div>
            <form action="{{route('dokter.swabupdate')}}" method="POST">
                @csrf
                <input type="hidden" name="code" value="{{$transactions->code}}">
            @foreach ($transactions->detail as $item)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Pasien {{$loop->iteration}}  @if($item->hasil_test != null)<span class="badge badge-success badge-pill"><i data-feather="check"></i></span>@endif</h5>
                        <div class="form-group row">
                          <label for="jenis_test" class="col-sm-2 col-form-label">Test</label>
                          <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="jenis_test" value="{{$item->name_test}}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="Nik" class="col-sm-2 col-form-label">NIK</label>
                          <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="Nik" value="{{$item->pasien->nik}}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                          <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="nama" value="{{$item->pasien->nama}}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                          <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="jenis_kelamin" value="{{$item->pasien->jenis_kelamin}}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="tanggal_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                          <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="tanggal_lahir" value="{{$item->pasien->tanggal_lahir}}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                          <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="tempat_lahir" value="{{$item->pasien->tempat_lahir}}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="umur" class="col-sm-2 col-form-label">Umur</label>
                          <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="umur" value="{{$item->pasien->umur}}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                          <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="alamat" value="{{$item->pasien->alamat}}">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="suhu_badan" class="col-sm-2 col-form-label">Suhu Badan</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="suhu_badan" name="suhu_badan[]" value="{{$item->suhu_badan}}" placeholder="Masukan suhu badan pasien">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="hasil_test" class="col-sm-2 col-form-label">Hasil Swab</label>
                          <div class="col-sm-10">
                            <select name="hasil_test[]" id="hasiltest" class="form-control" required>
                                <option value="">Pilih</option>
                                @if($item->name_test == "Antibody")
                                <option value="Reaktif IgM" @if($item->hasil_test == "Reaktif IgM") selected @endif>Reaktif IgM</option>
                                <option value="Non Reaktif IgM" @if($item->hasil_test == "Non Reaktif IgM") selected @endif>Non Reaktif IgM</option>
                                <option value="Reaktif IgG" @if($item->hasil_test == "Reaktif IgG") selected @endif>Reaktif IgG</option>
                                <option value="Non Reaktif IgG" @if($item->hasil_test == "Non Reaktif IgG") selected @endif>Non Reaktif IgG</option>
                                <option value="Reaktif IgM & IgG" @if($item->hasil_test == "Reaktif IgM & IgG") selected @endif>Reaktif IgM & IgG</option>

                                @else
                                <option value="NEGATIF/NON REAKTIF" @if($item->hasil_test == "Non-Reaktif" || $item->hasil_test == "NEGATIF" || $item->hasil_test == "NEGATIF/NON REAKTIF") selected @endif>NEGATIF/NON REAKTIF</option>
                                <option value="POSITIF/REAKTIF" @if($item->hasil_test == "POSITIF" || $item->hasil_test == "Reaktif" || $item->hasil_test == "POSITIF/REAKTIF") selected @endif>POSITIF/REAKTIF</option>
                                @if($item->name_test == "Swab PCR H+1" || $item->name_test == "Swab PCR Express" )
                                <option value="PCR H+1" @if($item->hasil_test == "PCR H+1" ) selected @endif>PCR H+1</option>
                                @endif
                                @endif
                            </select>
                          </div>
                          <input type="hidden" value="{{$item->id}}" name="id_detail[]">
                        </div>
                </div>
            </div>
            @endforeach
            <button class="btn btn-success" type="submit">Simpan</button>
            </form>
		</div>
	</div>
	<!-- /Row -->
</div>
@push('after-scripts')
@if ($message = Session::get('success'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Success!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-primary',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@endpush
<!-- /Container -->
@endsection
