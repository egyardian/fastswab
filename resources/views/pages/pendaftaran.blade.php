<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Pendaftaran</title>
    @include('includes.style')
    <link rel="stylesheet" href="{{asset('dropify/css/dropify.css')}}">
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
        }

        /* Firefox */
        input[type=number] {
        -moz-appearance: textfield;
        }
    </style>
</head>
<body style="background-color: #fff">
    <div class="container p-5">
        <div class="card mt-4 shadow">
            <div class="card-body">
                <div class="text-center mb-4">
                    <h5 class="card-title">Pendaftaran</h5>
                    <img src="{{ asset('assets/images/fastswab.png') }}" width="200px" alt="">
                    <h5 class="card-title">{{$lab->nama_lab}}</h5>
                </div>
                <form action="{{route('daftar')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="jenis_test">Jenis Test</label>
                        <select name="jenistest_id" id="jenis_test" class="form-control" required>
                            <option value="">-Pilih Jenis Test-</option>
                            @foreach ($jenistests as $jenistest)
                                <option value="{{$jenistest->id}}">{{$jenistest->name_test}} - Rp {{number_format($jenistest->price_test)}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        @if ($lab->type == 2)
                        <label for="">Addon</label>
                        @foreach ($addons as $item )
                        <div class="form-check">
                            <input class="form-check-input" name="addon[]" type="checkbox" value="{{$item->id}}" id="defaultCheck{{$loop->iteration}}">
                            <label class="form-check-label" for="defaultCheck{{$loop->iteration}}">
                                {{$item->name}} - Rp{{number_format($item->price)}}
                            </label>
                          </div>

                        @endforeach

                        @endif
                    </div>
                    <div class="form-row">
                        <input type="hidden" name="lab_id" value="{{$lab->id}}">
                        <input type="hidden" name="no_lab" value="{{$lab->no_lab}}">
                        <div class="form-group col-sm-6">
                          <label for="inputEmail4">Email</label>
                          <input type="email" class="form-control" id="inputEmail4" placeholder="Email" name="email" required>
                        </div>
                        <div class="form-group col-sm-6">
                          <label for="inputPassword4">No Hp (Whatsapp)</label>
                          <input type="number" class="form-control" id="inputPassword4" placeholder="Nomor Handphone" name="phone" required>
                        </div>
                        <small id="emailHelp" class="form-text text-muted">*isi email dan no handphone dengan benar untuk mendapatkan hasil digital (pdf)</small>
                    </div>
                    <div id="daftar">
                        <div class="form-group mt-2">
                          <label for="nik">NIK</label>
                          <input type="number" class="form-control" id="nik" placeholder="Masukan NIK" name="nik[]" required>
                        </div>
                        <div class="form-group">
                          <label for="nama">*Nama Lengkap</label>
                          <input type="text" class="form-control" id="nama" placeholder="Nama Lengkap" name="nama[]" required>
                        </div>
                        <div class="form-group">
                            <label for="jeniskelamin">*Jenis Kelamin</label>
                            <select name="jenis_kelamin[]" class="form-control" id="jeniskelamin" required>
                                <option value="L" selected>Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tanggal_lahir">Tanggal Lahir</label>
                            <input type="date" value="1996-01-01" class="form-control" id="tanggal_lahir" name="tanggal_lahir[]" required>
                        </div>
                        <div class="form-group">
                            <label for="tempan_lahir">Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir[]" required id="tempat_lahir">
                        </div>
                        {{-- <div class="form-group">
                            <label for="umur">Umur</label>
                            <input type="number" class="form-control" name="umur[]" required id="umur">
                        </div> --}}
                        <div class="form-group">
                            <label for="suhu_badan">Suhu Badan</label>
                            <input type="text" class="form-control" name="suhu_badan[]" required id="suhu_badan">
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat Lengkap (sesuai KTP)</label>
                            <textarea name="alamat[]" required id="alamat" cols="30" rows="5" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="fotoktp">Foto KTP</label>
                            <input type="file" class="dropify" name="fotoktp[]" accept="image/*" data-allowed-file-extensions="jpg jpeg png">
                        </div>
                    </div>
                    <div class="text-center mt-4" id="daftarbaru">
                        <button type="button" class="btn btn-dark tambahorang">Tambah Orang</button>
                        <button type="button" class="btn btn-danger" id="hapusorang">Hapus Orang</button>
                    </div>
                    <hr>
                    <div class="text-center">
                        <button type="submit" class="btn btn-dark" style="width: 100%">Daftar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('includes.scripts')
    <script>
        $(document).ready(function(){
            var daftar = 1;
            function add_daftar(){
                var n = daftar+1;
                var html = '<div  id="newdaftar'+daftar+'">'+
                            '<hr>'+
                                '<div class="form-group mt-2">'+
                                    '<label for="nik">NIK</label>'+
                                    '<input type="number" class="form-control" required id="nik" placeholder="Masukan NIK" name="nik[]">'+
                                '</div>'+
                                '<div class="form-group">'+
                                    '<label for="nama">*Nama Lengkap</label>'+
                                    '<input type="text" class="form-control" id="nama" placeholder="Nama Lengkap" name="nama[]" required>'+
                                '</div>'+
                                '<div class="form-group">'+
                                    '<label for="jeniskelamin">*Jenis Kelamin</label>'+
                                    '<select name="jenis_kelamin[]" class="form-control" id="jeniskelamin" required>'+
                                        '<option value="L" selected>Laki-Laki</option>'+
                                        '<option value="P">Perempuan</option>'+
                                    '</select>'+
                                '</div>'+
                                '<div class="form-group">'+
                                    '<label for="tanggal_lahir">Tanggal Lahir</label>'+
                                    '<input type="date" value="1996-01-01" class="form-control" id="tanggal_lahir" name="tanggal_lahir[]" required>'+
                                '</div>'+
                                '<div class="form-group">'+
                                    '<label for="tempan_lahir">Tempat Lahir</label>'+
                                    '<input type="text" class="form-control" name="tempat_lahir[]" required id="tempat_lahir">'+
                                '</div>'+
                                // '<div class="form-group">'+
                                //     '<label for="umur">Umur</label>'+
                                //     '<input type="number" class="form-control" name="umur[]" required id="umur">'+
                                // '</div>'+
                                '<div class="form-group">'+
                                    '<label for="suhu_badan">Suhu Badan</label>'+
                                    '<input type="text" class="form-control" name="suhu_badan[]" required id="suhu_badan">'+
                                '</div>'+
                                '<div class="form-group">'+
                                    '<label for="alamat">Alamat Lengkap (sesuai KTP)</label>'+
                                    '<textarea name="alamat[]" required id="alamat" cols="30" rows="5" class="form-control"></textarea>'+
                                '</div>'+
                                '<div class="form-group">'+
                                    '<label for="fotoktp">Foto KTP</label>'+
                                    '<input type="file" class="dropify" name="fotoktp[]" accept="image/*" data-allowed-file-extensions="jpg jpeg png">'+
                                '</div>'+
                        '</div>';

                                $('#daftarbaru').before(html);
                                $('#newdaftar'+daftar).slideDown('medium');

                                daftar++;
                                $('.dropify').dropify();

            }
            function deletedaftar(){
                daftar--;
                if(daftar<=1){
                    daftar=1;
                }
                $('#newdaftar'+daftar).slideUp('medium', function(){
                    $(this).remove();
                });
            }

            $('.tambahorang').click(function(){
                add_daftar();
            });
            $('#hapusorang').click(function(){
                deletedaftar();
            });
        });
    </script>
    <script src="{{asset('dropify/js/dropify.js')}}"></script>
    <script>
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();
        });
    </script>
</body>
</html>
