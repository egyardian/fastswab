@extends('layouts.index')
@section('title','Transaction')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
			<h2 class="hk-pg-title font-weight-600 mb-10">Transaction (ReOrder)</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="card mt-4 shadow">
        <div class="card-body">
            <div class="text-center mb-4">
                <h5 class="card-title">Daftar Ulang</h5>
                <small>pilih jenis test dan suhu badan saat ini saja</small>
            </div>
            <form action="{{route('admin.reordersubmit')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="jenis_test">Jenis Test</label>
                    <select name="jenistest_id" id="jenis_test" class="form-control" required>
                        <option value="">-Pilih Jenis Test-</option>
                        @foreach ($jenistests as $jenistest)
                            <option value="{{$jenistest->id}}">{{$jenistest->name_test}} - Rp {{number_format($jenistest->price_test)}}</option>
                        @endforeach
                    </select>
                </div>
                @if ($lab->type == 2)
                        <label for="">Addon</label>
                        @foreach ($addons as $item )
                        <div class="form-check">
                            <input class="form-check-input" name="addon[]" type="checkbox" value="{{$item->id}}" id="defaultCheck{{$loop->iteration}}">
                            <label class="form-check-label" for="defaultCheck{{$loop->iteration}}">
                                {{$item->name}} - Rp{{number_format($item->price)}}
                            </label>
                          </div>

                        @endforeach

                        @endif
                <div class="form-row">
                    <input type="hidden" name="lab_id" value="{{$lab->id}}">
                    <input type="hidden" name="no_lab" value="{{$lab->no_lab}}">
                    <div class="form-group col-sm-6">
                      <label for="inputEmail4">Email</label>
                      <input type="email" class="form-control" id="inputEmail4" placeholder="Email" name="email" value="{{$transaction->email}}" readonly>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="inputPassword4">No Hp (Whatsapp)</label>
                      <input type="number" class="form-control" id="inputPassword4" placeholder="Nomor Handphone" name="phone" value="{{$transaction->phone}}" readonly>
                    </div>
                    <small id="emailHelp" class="form-text text-muted">*isi email dan no handphone dengan benar untuk mendapatkan hasil digital (pdf)</small>
                </div>
                @foreach($transaction->detail as $pasien)
                <div id="daftar">
                    <br>
                    <h5>Pasien {{$loop->iteration}}</h5>
                    <div class="form-group mt-2">
                      <label for="nik">NIK</label>
                      <input type="number" class="form-control" id="nik" placeholder="Masukan NIK" name="nik[]" value="{{$pasien->pasien->nik}}">
                      <small id="emailHelp" class="form-text text-muted">*kosongkan jika tidak ada.</small>
                    </div>
                    <div class="form-group">
                      <label for="nama">*Nama Lengkap</label>
                      <input type="text" class="form-control" id="nama" placeholder="Nama Lengkap" name="nama[]" value="{{$pasien->pasien->nama}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="jeniskelamin">*Jenis Kelamin</label>
                        @if($pasien->pasien->jenis_kelamin == "L")
                        <input type="text" class="form-control" value="Laki-Laki" readonly>
                        <input type="hidden" class="form-control" name="jenis_kelamin[]" value="{{$pasien->pasien->jenis_kelamin}}">
                        @else
                        <input type="text" class="form-control" value="Perempuan" readonly>
                        <input type="hidden" class="form-control" name="jenis_kelamin[]" value="{{$pasien->pasien->jenis_kelamin}}">
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir">Tanggal Lahir</label>
                        <input type="date" value="{{$pasien->pasien->tanggal_lahir}}" class="form-control" id="tanggal_lahir" name="tanggal_lahir[]" readonly>
                    </div>
                    <div class="form-group">
                        <label for="tempan_lahir">Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat_lahir[]" readonly id="tempat_lahir" value="{{$pasien->pasien->tempat_lahir}}">
                    </div>
                    <div class="form-group">
                        <label for="suhu_badan">Suhu Badan</label>
                        <input type="text" class="form-control" name="suhu_badan[]" id="suhu_badan" required>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat Lengkap (sesuai KTP)</label>
                        <textarea name="alamat[]" readonly id="alamat" cols="30" rows="5" class="form-control">{{$pasien->pasien->alamat}}</textarea>
                    </div>
                    <label for="fotoktp">Foto KTP</label>
                    <div class="form-group">
                        <img src="{{asset($pasien->pasien->fotoktp)}}" alt="">
                        <input type="hidden" name="fotoktp[]" value="{{$pasien->pasien->fotoktp}}">
                    </div>
                </div>
                @endforeach
                <hr>
                <div class="text-center">
                    <button type="submit" class="btn btn-dark" style="width: 100%">Daftar Ulang</button>
                </div>
            </form>
        </div>
    </div>
	<!-- /Row -->
</div>


@push('after-scripts')


@endpush
<!-- /Container -->
@endsection
