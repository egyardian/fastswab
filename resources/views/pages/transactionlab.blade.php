@extends('layouts.index')
@section('title','Transaction')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
<!-- Container -->
<div class="container-fluid mt-4">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div class="px-50 py-19">
            <h2 class="hk-pg-title font-weight-600">Transaction {{$lab->nama_lab}}</h2>
            <small>{{$lab->no_lab}}</small>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
                <div class="card-body">
                    {{-- <a href="{{route('lab.create')}}" class="btn btn-primary mb-2">Create Lab</a> --}}
                    <div class="table-responsive">
                        <table class="table table-bordered" id="datable_1">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Pasien</th>
                                    <th>Grand Total</th>
                                    <th>Status</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>

<div class="modal fade" id="bayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Bayar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('admin.transactionpay')}}" method="POST">
            @csrf
            <div class="modal-body">
                <p class="text-center">Tandai sudah bayar dan buat invoice?</p>
                <input type="hidden" id="idtrans" name="idtrans">
            </div>
            {{-- <div class="form-group mx-4">
                <label for="">Addon</label>
                <select name="addon" id="" class="form-control">
                    <option value="">Tidak Ada</option>
                    @foreach ($addons as $addon)
                    <option value="{{$addon->id}}">{{$addon->name}} - Rp{{number_format($addon->price)}}</option>
                    @endforeach
                </select>
                <br>
                <label for="">Jumlah</label>
                <div class="input-group col-4">
                    <span class="input-group-prepend">
                        <button type="button" class="btn btn-outline-secondary btn-number" disabled="disabled" data-type="minus" data-field="qty">
                            <span class="fa fa-minus"></span>
                        </button>
                    </span>
                    <input type="text" name="qty" class="form-control input-number" value="1" min="1" max="10" readonly style="width: 20%">
                    <span class="input-group-append">
                        <button type="button" class="btn btn-outline-secondary btn-number" data-type="plus" data-field="qty">
                            <span class="fa fa-plus"></span>
                        </button>
                    </span>

                </div>
            </div> --}}
            <div class="modal-footer justify-content-between">
                <button type="submit" class="btn btn-success">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
    </div>
  </div>
<div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('admin.transactiondelete')}}" method="POST">
            @csrf
            <div class="modal-body">
                <p class="text-center">Hapus Data Ini?</p>
                <input type="hidden" id="idtrans" name="idtrans">
            </div>
            <div class="modal-footer justify-content-between">
                <button type="submit" class="btn btn-danger">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
    </div>
  </div>


@push('after-scripts')
    <!-- Data Table JavaScript -->
    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#datable_1').DataTable({
                processing:true,
                serverSide:true,
                ordering:true,
                ajax:{
                    url:'{!! url()->current() !!}',
                },
                columns:[
                    { data:'nama',name:'nama'},
                    { data:'email',name:'email'},
                    { data:'phone',name:'phone'},
                    {
                        data:'pasien',
                        name:'pasien',
                        orderable:'false',
                        searchable: 'false',
                    },
                    { data:'amount',name:'amount'},
                    { data:'status',name:'status'},
                    {
                        data:'action',
                        name:'action',
                        orderable:'false',
                        searchable: 'false',
                    },

                ]


            });
    });
    </script>
    <script>
        $('#bayar').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var idtrans = button.data('id')
            var modal = $(this)
            modal.find('.modal-body #idtrans').val(idtrans);
        })
        $('#hapus').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var idtrans = button.data('id')
            var modal = $(this)
            modal.find('.modal-body #idtrans').val(idtrans);
        })
        $('#pindah').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var idtrans = button.data('id')
            var modal = $(this)
            modal.find('.modal-body #idtrans').val(idtrans);
        })
    </script>

@if ($message = Session::get('success'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-primary',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@if ($message = Session::get('error'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-danger',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif

<script>

$('.btn-number').click(function(e){
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            }
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {

    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
});

$(".input-number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
</script>
@endpush
<!-- /Container -->
@endsection
