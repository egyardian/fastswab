<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Pendaftaran</title>
    @include('includes.style')
</head>
<body style="background-color: #fff">
    <div class="container">
        <div class="card mt-4 shadow">
            <div class="card-body text-center">
                <img class="mb-2" src="{{ asset('assets/images/fastswab.png') }}" width="100px" alt=""> <br>
                {!! QrCode::size(230)->generate($code); !!}
                <p class="mt-1">{{$code}}</p>
                <p class="mt-2">Selamat Anda Berhasil Daftar.</p>
                <p>Mohon tunjukan qrcode atau nomor ini jika di minta</p>
                <p>Agar mempercepat proses Transaksi dan Swab Test</p>
            </div>
        </div>
        <div class="text-center">
            <a href="" class="btn btn-primary">Kembali</a>
        </div>
    </div>
    @include('includes.scripts')
</body>
</html>
