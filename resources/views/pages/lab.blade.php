@extends('layouts.index')
@section('title','Data - Lab')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
			<h2 class="hk-pg-title font-weight-600 mb-10">Lab</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
                <div class="card-body">
                    <a href="{{route('lab.create')}}" class="btn btn-primary mb-2">Create Lab</a>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="datable_1">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No Lab</th>
                                    <th>Nama Lab</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($labs as $lab)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$lab->no_lab}}</td>
                                    <td>{{$lab->nama_lab}}</td>
                                        <td>
                                            <a href="{{route('lab.edit',$lab->id)}}" class="btn btn-warning px-2"><i class="fa fa-edit"></i>Edit</a>
                                            <button type="button" class="btn btn-danger px-2" data-id="{{$lab->id}}" data-toggle="modal" data-target="#deletelab"><i class="fa fa-trash"></i>Hapus</button>
                                            <a href="{{route('super.downloadqrcode',$lab->no_lab)}}" class="btn btn-success px-2">QrCode</a>
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>

<div class="modal fade" id="deletelab" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete Lab</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('lab.destroy','deletelab')}}" method="POST">
            @csrf
            @method('delete')
            <div class="modal-body">

                <p class="text-center">Delete Lab ini?</p>
                <input type="hidden" id="idlab" name="idlab">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>
      </div>
    </div>
  </div>

@push('after-scripts')
    <!-- Data Table JavaScript -->
    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#datable_1').DataTable({
                responsive: true,
                autoWidth: false,
                language: { search: "",
                searchPlaceholder: "Search",
                sLengthMenu: "_MENU_items"

                }
            });
    });
    </script>
    <script>
        $('#deletelab').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var idlab = button.data('id')
            var modal = $(this)
            modal.find('.modal-body #idlab').val(idlab);
        })
    </script>

@if ($message = Session::get('success'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-primary',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@if ($message = Session::get('error'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-danger',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@endpush
<!-- /Container -->
@endsection
