@extends('layouts.index')
@section('title','Create - Lab')
@section('content')

@push('after-style')

<link rel="stylesheet" href="{{asset('dropify/css/dropify.css')}}">


@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
			<h2 class="hk-pg-title font-weight-600 mb-10">Create Lab</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('lab.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                          <label for="no_lab">No Lab</label>
                          <input type="text" class="form-control @error('no_lab') is-invalid @enderror" id="no_lab" placeholder="Masukan No Lab" name="no_lab">
                          @error('no_lab')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                          @enderror
                        </div>
                        <div class="form-group">
                          <label for="nama_lab">Nama Lab</label>
                          <input type="text" class="form-control @error('nama_lab') is-invalid @enderror" id="nama_lab" placeholder="Masukan Nama Lab" name="nama_lab">
                          @error('nama_lab')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                        <div class="form-group">
                          <label for="provinsi">Provinsi</label>
                            <select name="provinsi" id="provinsi" class="form-control @error('provinsi') is-invalid @enderror">
                                <option value="">-Pilih Provinsi-</option>
                                @foreach ($provinsi as $prov)
                                    <option value="{{$prov->nama}}" idprov="{{$prov->id}}">{{$prov->nama}}</option>
                                @endforeach
                            </select>
                        @error('provinsi')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                        <div class="form-group">
                          <label for="kota">Kota/Kabupaten</label>
                            <select name="kota" id="kota" class="form-control @error('kota') is-invalid @enderror">
                                <option value="">-Pilih Kota-</option>
                            </select>
                        @error('kota')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                        <div class="form-group">
                          <label for="kota">Kecamatan</label>
                            <select name="kecamatan" id="kecamatan" class="form-control @error('kecamatan') is-invalid @enderror">
                                <option value="">-Pilih Kecamatan-</option>
                            </select>
                        @error('kecamatan')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea name="alamat" id="alamat" cols="30" rows="5" class="form-control @error('alamat') is-invalid @enderror" ></textarea>
                            @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="pengawas">Pengawas</label>
                            <input type="text" class="form-control @error('pengawas') is-invalid @enderror" id="pengawas" placeholder="Masukan Nama Pengawas Lab" name="pengawas">
                            @error('pengawas')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                        </div>
                        <div class="form-group">
                            <label for="tanda_tangan">Tanda Tangan (untuk menampilkan di hasil swab)</label>
                            <div style="width: 50%">
                                <input type="file" class="dropify" name="tanda_tangan" required accept="image/*" data-allowed-file-extensions="jpg jpeg png" >
                            </div>
                            @error('tanda_tangan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                        </div>
                        <div class="form-group">
                            <label for="tanda_tangan">Alamat Header (menampilkan di atas output)</label>
                            <div style="width: 100%">
                                <input type="file" class="dropify" name="header_template" required accept="image/*" data-allowed-file-extensions="jpg jpeg png" >
                            </div>
                            @error('header_template')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                        </div>
                        <label for="type">Type</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="type" id="exampleRadios1" value="1" checked>
                            <label class="form-check-label" for="exampleRadios1">
                             DriveThru
                            </label>
                          </div>
                          <div class="form-check mb-2">
                            <input class="form-check-input" type="radio" name="type" id="exampleRadios2" value="2">
                            <label class="form-check-label" for="exampleRadios2">
                              Home Service
                            </label>
                          </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>

@push('after-scripts')
<script type="text/javascript">
    $(document).ready(function() {
    $('select[name="provinsi"]').on('change', function(){
        var provinsiID = $("option:selected", this).attr("idprov");
        if(provinsiID) {
            $.ajax({
                url: "/getkota/"+provinsiID,
                type:"GET",
                dataType:"json",
                success:function(result) {
                    console.log(result);
                    $('select[name="kota"]').empty();
                    $('select[name="kota"]').append('<option value="" idkota="">-Pilih Kota-</option>');
                    $.each(result.kota_kabupaten, function(id,nama){
                        $('select[name="kota"]').append('<option value="'+ nama.nama +'" idkota="'+ nama.id +'">' + nama.nama + '</option>');
                    });
                },
            });
        } else {
            $('select[name="kota"]').empty();
            $('select[name="kota"]').append('<option value="">-Pilih Kota-</option>');

        }
    });
    $('select[name="kota"]').on('change', function(){
        var kotaID = $("option:selected", this).attr("idkota");
        if(kotaID) {
            $.ajax({
                url: "/getkecamatan/"+kotaID,
                type:"GET",
                dataType:"json",
                success:function(result) {
                    $('select[name="kecamatan"]').empty();
                    $('select[name="kecamatan"]').append('<option value="">-Pilih Kecamatan-</option>');
                    $.each(result.kecamatan, function(key, value){
                        $('select[name="kecamatan"]').append('<option value="'+ value.nama +'">' + value.nama + '</option>');
                    });
                },
            });
        } else {
            $('select[name="kecamatan"]').empty();
            $('select[name="kecamatan"]').append('<option value="">-Pilih Kecamatan-</option>');
        }
    });
});
</script>
<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    </script>
<script src="{{asset('dropify/js/dropify.js')}}"></script>
<script>
    $(document).ready(function(){
        // Basic
        $('.dropify').dropify();
    });
</script>
@endpush
<!-- /Container -->
@endsection
