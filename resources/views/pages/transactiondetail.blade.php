@extends('layouts.index')
@section('title','Transaction - Detail')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
            <h2 class="hk-pg-title font-weight-600 mb-10">Detail</h2>
            <small>{{$transactions->code}}</small>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            <div class="mb-2">
                <a href="{{route('admin.transactionlab',$lab->id)}}" class="btn btn-primary">Kembali</a>
                <a href="{{route('admin.transactionviewinvoice',$transactions->code)}}" class="btn btn-success">Invoice</a>
            </div>
            @foreach ($transactions->detail as $item)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Pasien {{$loop->iteration}}</h5>
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>Nik</th>
                                <td>{{$item->pasien->nik}}</td>
                            </tr>
                            <tr>
                                <th>Nama</th>
                                <td>{{$item->pasien->nama}}</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>{{$item->pasien->jenis_kelamin}}</td>
                            </tr>
                            <tr>
                                <th>Tanggal Lahir</th>
                                <td>{{$item->pasien->tanggal_lahir}}</td>
                            </tr>
                            <tr>
                                <th>Tempat Lahir</th>
                                <td>{{$item->pasien->tempat_lahir}}</td>
                            </tr>
                            <tr>
                                <th>Umur</th>
                                <td>{{$item->pasien->umur}}</td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>{{$item->pasien->alamat}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            @endforeach

		</div>
	</div>
	<!-- /Row -->
</div>
<!-- /Container -->
@endsection
