@extends('layouts.index')
@section('title','Edit - User')
@section('content')

@push('after-style')


@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
			<h2 class="hk-pg-title font-weight-600 mb-10">Edit User</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Edit User</h5>
                    <form action="{{route('user.update',$user->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Masukan Nama" name="name" value="{{$user->name}}">
                          @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                          @enderror
                        </div>
                        <div class="form-group">
                          <label for="username">Username</label>
                          <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="Masukan Username" name="username" value="{{$user->username}}">
                          @error('username')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                        <div class="form-group">
                            <label for="Lab">Lab</label>
                            <select name="lab" id="Lab" class="form-control @error('lab') is-invalid @enderror">
                                <option value="">Pilih Lab</option>
                                @foreach ($lab as $item)
                                    <option @if($user->lab_id == $item->id) selected @endif value="{{$item->id}}">{{$item->nama_lab}}</option>
                                @endforeach
                            </select>
                            @error('lab')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="role" id="role1" value="2" @if($user->role == 2) checked @endif>
                            <label class="form-check-label" for="role1">
                              Admin
                            </label>
                          </div>
                          <div class="form-check mb-2">
                            <input class="form-check-input" type="radio" name="role" id="role2" value="3"@if($user->role == 3) checked @endif>
                            <label class="form-check-label" for="role2">
                              Dokter
                            </label>
                          </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                    <hr>
                    <h5 class="card-title">Change password</h5>
                    <form action="{{route('super.changepassword',$user->id)}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Masukan password baru" name="password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                           @enderror
                          </div>
                        <button type="submit" class="btn btn-primary">Change</button>
                    </form>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>

@push('after-scripts')
@if ($message = Session::get('success'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-primary',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>
@endif
@endpush
<!-- /Container -->
@endsection
