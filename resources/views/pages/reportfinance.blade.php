@extends('layouts.index')
@section('title','FastSwab - Report')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/vendors/datepicker/css/bootstrap-datepicker.css') }}">

@endpush
<!-- Container -->
<div class="container-fluid mt-4">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div class="px-50 py-19">
            <h2 class="hk-pg-title font-weight-600 mb-10">FastSwab - Report</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('financeExport') }}" method="GET">
                        <div class="form-group">
                            <label for="jenistest">Jenis Test</label>
                            <select name="jenis" class="form-control" id="jenis">
                                <option value="all">Semua</option>
                                @foreach ($jenis_test as $item)
                                <option value="{{ $item->name_test }}">{{ $item->name_test }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Pilih Tanggal</label>
                            <input type="text" class="form-control" name="dates">

                            <input type="hidden" id="start" name="start">
                            <input type="hidden" id="end" name="end">
                        </div>
                        <button type="submit" class="btn btn-primary my-2">Export Excel</button>
                    </form>
                </div>
            </div>
            <div class="data-index">

            </div>
		</div>
	</div>
	<!-- /Row -->
</div>



@push('after-scripts')
    <!-- Data Table JavaScript -->
    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script>
            $('input[name="dates"]').daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                    }
            },function(start, end, label) {
                    var start = start.format('YYYY-MM-DD');
                    var end = end.format('YYYY-MM-DD');
                    var jenis = $('#jenis').val();
                    $('#start').val(start);
                    $('#end').val(end);
                    fetch_data(start,end,jenis);
            });

            function fetch_data(start,end,jenis)
            {
                $.ajax({
                        url:"{{route('financeFilter')}}"+"?start="+start+"&end="+end+"&jenis="+jenis,
                        success:function(data)
                        {
                            $('.data-index').html('')
                            $('.data-index').html(data)
                        }
                    })
            }

</script>
@if ($message = Session::get('success'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-primary',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@if ($message = Session::get('error'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-danger',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@endpush
<!-- /Container -->
@endsection
