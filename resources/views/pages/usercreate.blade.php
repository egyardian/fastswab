@extends('layouts.index')
@section('title','Create - User')
@section('content')

@push('after-style')


@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
			<h2 class="hk-pg-title font-weight-600 mb-10">Create User</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('user.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Masukan Nama" name="name">
                          @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                          @enderror
                        </div>
                        <div class="form-group">
                          <label for="username">Username</label>
                          <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="Masukan Username" name="username">
                          @error('username')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                        <div class="form-group">
                          <label for="password">Password</label>
                          <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password" name="password">
                          @error('password')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                        </div>
                        <div class="form-group">
                          <label for="confirmpassword">Confirmation Password</label>
                          <input type="password" class="form-control" id="confirmpassword" placeholder="Confirm Password" name="password_confirmation">
                        </div>
                        <div class="form-group">
                            <label for="Lab">Lab</label>
                            <select name="lab" id="Lab" class="form-control @error('lab') is-invalid @enderror">
                                <option value="">Pilih Lab</option>
                                @foreach ($lab as $item)
                                    <option value="{{$item->id}}">{{$item->nama_lab}}</option>
                                @endforeach
                            </select>
                            @error('lab')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                        @enderror
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="role" id="role1" value="2" checked>
                            <label class="form-check-label" for="role1">
                              Admin
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="role" id="role2" value="3">
                            <label class="form-check-label" for="role2">
                              Dokter
                            </label>
                          </div>
                          <div class="form-check mb-2">
                            <input class="form-check-input" type="radio" name="role" id="role2" value="4">
                            <label class="form-check-label" for="role2">
                              Semi-SuperAdmin
                            </label>
                          </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>

@push('after-scripts')

@endpush
<!-- /Container -->
@endsection
