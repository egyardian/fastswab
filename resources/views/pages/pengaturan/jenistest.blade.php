@extends('layouts.index')
@section('title','Data - Jenis Test')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
			<h2 class="hk-pg-title font-weight-600 mb-10">Jenis Test</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
                <div class="card-body">
                    <a href="{{route('addviewjenistest')}}" class="btn btn-primary mb-2">Tambah Jenis Test</a>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="datable_1">
                            <thead>
                                <tr>
                                    <th>Nama Test</th>
                                    <th>Harga</th>
                                    <th>Berlaku</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($jenistests as $jenistest)
                                <tr>
                                    <td>{{$jenistest->name_test}}</td>
                                    <td>Rp {{number_format($jenistest->price_test)}}</td>
                                    <td>{{$jenistest->expire_day}} Hari</td>
                                    @if($jenistest->status == "1")
                                    <td><span class="badge badge-success">Aktif</span></td>
                                    @else
                                    <td><span class="badge badge-danger">Non-Aktif</span></td>
                                    @endif
                                        <td>
                                            <a href="{{route('editjenistest',$jenistest->id)}}" class="btn btn-warning px-2"><i class="fa fa-edit"></i>Edit</a>
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>



@push('after-scripts')
    <!-- Data Table JavaScript -->
    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#datable_1').DataTable({
                responsive: true,
                autoWidth: false,
                language: { search: "",
                searchPlaceholder: "Search",
                sLengthMenu: "_MENU_items"

                }
            });
    });
    </script>

@if ($message = Session::get('success'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-primary',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@if ($message = Session::get('error'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-danger',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@endpush
<!-- /Container -->
@endsection
