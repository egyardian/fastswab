@extends('layouts.index')
@section('title','Edit - Addon')
@section('content')

@push('after-style')

<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
</style>


@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
			<h2 class="hk-pg-title font-weight-600 mb-10">Edit Addon</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('updateaddon',$addon->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                          <label for="name">Nama Test</label>
                          <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Masukan Nama Addon" name="name" value="{{$addon->name}}">
                          @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                          @enderror
                        </div>
                        <div class="form-group">
                          <label for="price">Harga Addon</label>
                          <input type="number" class="form-control @error('price') is-invalid @enderror" id="price" placeholder="Masukan Harga Test" name="price" value="{{$addon->price}}">
                          @error('price')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" id="exampleRadios1" value="1" @if($addon->status == "1") checked @endif>
                                <label class="form-check-label" for="exampleRadios1">
                                  Aktif
                                </label>
                              </div>
                              <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" id="exampleRadios2" value="0" @if($addon->status == "0") checked @endif>
                                <label class="form-check-label" for="exampleRadios2">
                                  Non-Aktif
                                </label>
                              </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>

@push('after-scripts')

@endpush
<!-- /Container -->
@endsection
