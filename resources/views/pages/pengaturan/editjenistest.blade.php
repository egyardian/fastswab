@extends('layouts.index')
@section('title','Edit - Jenis Test')
@section('content')

@push('after-style')

<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
</style>


@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
			<h2 class="hk-pg-title font-weight-600 mb-10">Edit Jenis Test</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('updatejenistest',$jenistest->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                          <label for="name_test">Nama Test</label>
                          <input type="text" class="form-control @error('name_test') is-invalid @enderror" id="name_test" placeholder="Masukan Nama Test" name="name_test" value="{{$jenistest->name_test}}">
                          @error('name_test')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                          @enderror
                        </div>
                        <div class="form-group">
                          <label for="price_test">Harga Test</label>
                          <input type="number" class="form-control @error('price_test') is-invalid @enderror" id="price_test" placeholder="Masukan Harga Test" name="price_test" value="{{$jenistest->price_test}}">
                          @error('price_test')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                        <div class="form-group">
                          <label for="expire_day">Masa Berlaku</label>
                          <input type="number" class="form-control @error('expire_day') is-invalid @enderror" id="expire_day" placeholder="Masukan jumlah hari masa berlaku" name="expire_day" value="{{$jenistest->expire_day}}">
                          @error('expire_day')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                         @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" id="exampleRadios1" value="1" @if($jenistest->status == "1") checked @endif>
                                <label class="form-check-label" for="exampleRadios1">
                                  Aktif
                                </label>
                              </div>
                              <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" id="exampleRadios2" value="0" @if($jenistest->status == "0") checked @endif>
                                <label class="form-check-label" for="exampleRadios2">
                                  Non-Aktif
                                </label>
                              </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>

@push('after-scripts')

@endpush
<!-- /Container -->
@endsection
