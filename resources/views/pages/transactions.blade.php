@extends('layouts.index')
@section('title','Transaction')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
			<h2 class="hk-pg-title font-weight-600 mb-10">Transaction (Pilih Lab)</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            <div class="row">
                @foreach ($labs as $lab)
                <div class="col-md-4">
                    <a href="{{route('admin.transactionlab',$lab->id)}}">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="text-center">{{$lab->nama_lab}}</h5>
                                <p class="text-center">{{$lab->no_lab}}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>


@push('after-scripts')


@endpush
<!-- /Container -->
@endsection
