@extends('layouts.index')
@section('title','Transaction - Invoice')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
            <h2 class="hk-pg-title font-weight-600 mb-10">Invoice</h2>
            <small>{{$transactions->code}}</small>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            <div class="mb-2">
                <a href="{{route('admin.transactiondetail',$transactions->code)}}" class="btn btn-primary">Kembali</a>
                <a href="{{route('admin.transactioninvoice',$transactions->code)}}" target="_blank" class="btn btn-danger">PDF to print</a>
            </div>
            <section class="hk-sec-wrapper hk-invoice-wrap pa-35">
                <div class="invoice-from-wrap">
                    <div class="row">
                        <div class="col-md-7 mb-20">
                            <img class="img-fluid invoice-brand-img d-block mb-20" src="{{ asset('assets/images/fastswab.png') }}" alt="brand" width="200px" />
                            <address>
                                    <span class="d-block">fastswab@gmail.com</span>
                                    <span class="d-block">Hotline: 081234502205</span>
                                </address>
                        </div>
                        <div class="col-md-5 mb-20">
                            <h4 class="mb-35 font-weight-600">Invoice</h4>
                            <span class="d-block">Tanggal: <span class="pl-10 text-dark">{{date('d-m-Y H:i:s')}}</span></span>
                            <span class="d-block">Invoice #<span class="pl-10 text-dark">{{$transactions->code}}</span></span>
                        </div>
                    </div>
                </div>
                <hr class="mt-0">
                <div class="invoice-to-wrap pb-20">
                            <span class="d-block text-uppercase mb-5 font-13">TO:</span>
                            <address>
                                    <span class="d-block">Nama: {{$transactions->nama}}</span>
                                    <span class="d-block">Email: {{$transactions->email}}</span>
                                    <span class="d-block">No Hp: {{$transactions->phone}}</span>
                                </address>
                </div>
                <div class="invoice-details">
                    <div class="table-wrap">
                        <div class="table-responsive">
                            <table class="table table-striped table-border mb-0">
                                <thead>
                                    <tr>
                                        <th class="w-70">Test</th>
                                        <th class="text-right">Jumlah(test)</th>
                                        <th class="text-right">Harga</th>
                                        <th class="text-right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="w-70">{{$transactions->detail->first()->name_test}}</td>
                                        <td class="text-right">{{count($transactions->detail)}}</td>
                                        <td class="text-right">Rp.{{number_format($transactions->price_test)}}</td>
                                        <td class="text-right">Rp.{{number_format($transactions->amount)}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <small>Tambahan: {!!$transactions->note!!}</small>
                    <br>
                    <br>
                    <div class="invoice-sign-wrap text-right py-20">
                        {{-- <img class="img-fluid d-inline-block" src="{{asset('assets/dist/img/signature.png')}}" alt="sign" /> --}}
                        <span class="d-block text-light font-14">Admin Approval</span>
                    </div>
                </div>
            </section>

		</div>
	</div>
	<!-- /Row -->
</div>
<!-- /Container -->
@endsection
