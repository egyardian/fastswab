@extends('layouts.index')
@section('title','FastSwab - Report')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
<!-- Container -->
<div class="container-fluid mt-4">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div class="px-50 py-19">
            <h2 class="hk-pg-title font-weight-600 mb-10">FastSwab - Report</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
                <div class="card-body">
                    <form action="{{ route('report') }}" method="get">
                        @csrf
                        <div class="form-group row">
                            <div class="col-4">
                                <select name="range" class="form-control" id="">
                                    <option value="6" @if(app('request')->input('range') == '6') selected @endif class="text-capitalize">All Time</option>
                                    <option value="1" @if(app('request')->input('range') == '1') selected @endif class="text-capitalize">Today</option>
                                    <option value="2" @if(app('request')->input('range') == '2') selected @endif class="text-capitalize">This Week</option>
                                    <option value="3" @if(app('request')->input('range') == '3') selected @endif class="text-capitalize">Last 7 Days</option>
                                    <option value="4" @if(app('request')->input('range') == '4') selected @endif class="text-capitalize">This Month ({{ \Carbon\Carbon::now()->format('F')}})</option>
                                    <option value="5" @if(app('request')->input('range') == '5') selected @endif class="text-capitalize">Last 30 Days</option>
                                </select>
                            </div>
                            <div class="col-4">
                                <select name="lab_id" class="form-control" id="">
                                    <option value="">All Lab</option>
                                    @foreach ($lab as $lb)
                                        <option value="{{ $lb->id }}" @if(app('request')->input('lab_id') == $lb->id) selected @endif>{{ $lb->nama_lab }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-3">
                                <button type="submit" class="btn btn-primary">Filter</button>
                            </div>
                        </div>
                    </form>
                    {{-- <form action="{{ route('printout') }}" method="get">
                        @csrf
                        <input type="hidden" name="range" value="{{ app('request')->input('range') }}">
                        <input type="hidden" name="lab_id" value="{{ app('request')->input('lab_id') }}">
                        <button type="submit" class="btn btn-danger">Download Printout</button>
                    </form> --}}
                    {{-- <div class="row mb-2">
                        <div class="col-3">
                            <p>Antigen</p>
                            <p>PCR</p>
                            <p>Antibody</p>
                        </div>
                        <div class="col-3">
                            <p>Positif : {{ $antigenpositif }} Negatif : {{ $antigennegatif }}</p>
                            <p>Positif : {{ $pcrpositif + $pcrexpresspositif}} Negatif : {{ $pcrnegatif + $pcrexpressnegatif}}</p>
                            <p>Reaktif : {{ $antibodyreaktifigm + $antibodyreaktifigg }} Non Reaktif : {{ $antibodynonreaktifigm + $antibodynonreaktifigg }} </p>
                        </div>
                    </div> --}}
                    {{-- <a href="{{route('lab.create')}}" class="btn btn-primary mb-2">Create Lab</a> --}}
                    <div class="table-responsive">
                        <table class="table table-bordered" id="datable_1">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>JK</th>
                                    <th>Tgl Lahir</th>
                                    <th>Alamat</th>
                                    <th>Lab</th>
                                    <th>Test</th>
                                    <th>Hasil</th>
                                    <th>Tgl Test</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($transactions as $item)
                                <tr>
                                    <td>{{ $item->pasien->nama }}</td>
                                    <td>{{ $item->pasien->jenis_kelamin }}</td>
                                    <td>{{ $item->pasien->tanggal_lahir }}</td>
                                    <td>{{ $item->pasien->alamat }}</td>
                                    <td>{{ $item->transaction->lab->nama_lab }}</td>
                                    <td>{{ $item->name_test }}</td>
                                    <td
                                    @if($item->hasil_test == "POSITIF" || $item->hasil_test == "Reaktif" || $item->hasil_test == "Reaktif IgM" || $item->hasil_test == "Reaktif IgG")
                                    class = "bg-red"
                        @endif>{{ $item->hasil_test }}</td>
                                    <td>{{ $item->created_at->format('d-m-Y H:i:s') }}</td>
                                    <td>
                                        <a href="{{ route('hasilsatu',$item->id) }}" class="btn btn-danger px-2"><i data-feather="printer"></i>PDF</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>



@push('after-scripts')
    <!-- Data Table JavaScript -->
    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#datable_1').DataTable({
                responsive: false,
                autoWidth: false,
                language: { search: "",
                searchPlaceholder: "Search",
                sLengthMenu: "_MENU_items"

                },
                dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
            });
    });
    </script>

@if ($message = Session::get('success'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-primary',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@if ($message = Session::get('error'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-danger',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@endpush
<!-- /Container -->
@endsection
