@extends('layouts.index')
@section('title','Home')
@section('content')
<!-- Container -->
<div class="container mt-xl-50 mt-sm-30 mt-15">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div>
			<h2 class="hk-pg-title font-weight-600">Dashboard</h2>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
            @if (Auth::user()->role == 1)
			<div class="hk-row">
				<div class="col-sm-12">
                <div class="card-group hk-dash-type-2">
                        <div class="card card-sm">
                            <div class="card-body">
                                <div class="d-flex justify-content-between mb-5">
                                    <div>
                                        <span class="d-block font-15 text-secondary font-weight-500">Total Pasien</span>
                                    </div>
                                </div>
                                <div>
                                    <span class="d-block display-4 text-secondary mb-5">{{$pasien}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="card card-sm">
                            <div class="card-body">
                                <div class="d-flex justify-content-between mb-5">
                                    <div>
                                        <span class="d-block font-15 text-success font-weight-500">Total Pendapatan</span>
                                    </div>
                                </div>
                                <div>
                                    <span class="d-block display-4 text-success mb-5">Rp.{{number_format($total)}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="card card-sm">
                            <div class="card-body">
                                <div class="d-flex justify-content-between mb-5">
                                    <div>
                                        <span class="d-block font-15 text-info font-weight-500">Total Pendapatan per Hari ini</span>
                                    </div>
                                </div>
                                <div>
                                    <span class="d-block display-4 text-info mb-5">Rp.{{number_format($totalhariini)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
            @endif
            <div class="hk-row">
                <div class="col-xl-6">
                    <section class="hk-sec-wrapper">
                        <h6 class="hk-sec-title">Pasien Antibody (Jumlah Pasien: {{$jumlahantibody}})</h6>
                        <div class="row">
                            <div class="col-sm">
                                <div id="antibody" class="echart" style="height:294px;"></div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-xl-6">
                    <section class="hk-sec-wrapper">
                        <h6 class="hk-sec-title">Pasien Swab Antigen (Jumlah Pasien: {{$jumlahantigen}})</h6>
                        <div class="row">
                            <div class="col-sm">
                                <div id="antigen" class="echart" style="height:294px;"></div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="hk-row">
                <div class="col-xl-6">
                    <section class="hk-sec-wrapper">
                        <h6 class="hk-sec-title">Pasien Swab PCR (Jumlah Pasien: {{$jumlahpcr}})</h6>
                        <div class="row">
                            <div class="col-sm">
                                <div id="swabpcr" class="echart" style="height:294px;"></div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-xl-6">
                    <section class="hk-sec-wrapper">
                        <h6 class="hk-sec-title">Pasien PCR Express (Jumlah Pasien: {{$jumlahpcrexpress}})</h6>
                        <div class="row">
                            <div class="col-sm">
                                <div id="pcrexpress" class="echart" style="height:294px;"></div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>


@push('after-scripts')
    <script src="{{asset('assets/dist/js/piecharts-data.js')}}"></script>
    <script>
        if( $('#antibody').length > 0 ){
		var antibody = echarts.init(document.getElementById('antibody'));

		var option1 = {
			tooltip: {
				show: true,
				backgroundColor: '#fff',
				borderRadius:6,
				padding:6,
				axisPointer:{
					lineStyle:{
						width:0,
					}
				},
				textStyle: {
					color: '#324148',
					fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"',
					fontSize: 12
				}
			},
			series: [
				{
					name:'',
					type:'pie',
					radius : '60%',
					color: ['#00acf0', '#ffbf36', '#f83f37', '#22af47'],
					data:[
						{value:{{$antibodynonreaktifigm}}, name:'NonReaktif IgG'},
						{value:{{$antibodyreaktifigg}}, name:'Reaktif IgG'},
						{value:{{$antibodyreaktifigm}}, name:'Reaktif IgM'},
						{value:{{$antibodynonreaktifigm}}, name:'NonReaktif IgM'},
					],
				}
			]
		};
		antibody.setOption(option1);
		antibody.resize();
	}
        if( $('#antigen').length > 0 ){
		var antigen = echarts.init(document.getElementById('antigen'));

		var option1 = {
			tooltip: {
				show: true,
				backgroundColor: '#fff',
				borderRadius:6,
				padding:6,
				axisPointer:{
					lineStyle:{
						width:0,
					}
				},
				textStyle: {
					color: '#324148',
					fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"',
					fontSize: 12
				}
			},
			series: [
				{
					name:'',
					type:'pie',
					radius : '60%',
					color: [ '#00acf0', '#f83f37'],
					data:[
						{value:{{$antigennegatif}}, name:'NEGATIF'},
						{value:{{$antigenpositif}}, name:'POSITIF'},
					],
				}
			]
		};
		antigen.setOption(option1);
		antigen.resize();
	}
        if( $('#swabpcr').length > 0 ){
		var swabpcr = echarts.init(document.getElementById('swabpcr'));

		var option1 = {
			tooltip: {
				show: true,
				backgroundColor: '#fff',
				borderRadius:6,
				padding:6,
				axisPointer:{
					lineStyle:{
						width:0,
					}
				},
				textStyle: {
					color: '#324148',
					fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"',
					fontSize: 12
				}
			},
			series: [
				{
					name:'',
					type:'pie',
					radius : '60%',
					color: [ '#00acf0', '#f83f37'],
					data:[
						{value:{{$pcrnegatif}}, name:'NEGATIF'},
						{value:{{$pcrpositif}}, name:'POSITIF'},
					],
				}
			]
		};
		swabpcr.setOption(option1);
		swabpcr.resize();
	}
        if( $('#pcrexpress').length > 0 ){
		var pcrexpress = echarts.init(document.getElementById('pcrexpress'));

		var option1 = {
			tooltip: {
				show: true,
				backgroundColor: '#fff',
				borderRadius:6,
				padding:6,
				axisPointer:{
					lineStyle:{
						width:0,
					}
				},
				textStyle: {
					color: '#324148',
					fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"',
					fontSize: 12
				}
			},
			series: [
				{
					name:'',
					type:'pie',
					radius : '60%',
					color: [ '#00acf0', '#f83f37'],
					data:[
						{value:{{$pcrexpressnegatif}}, name:'NEGATIF'},
						{value:{{$pcrexpresspositif}}, name:'POSITIF'},
					],
				}
			]
		};
		pcrexpress.setOption(option1);
		pcrexpress.resize();
	}
    </script>
@endpush
<!-- /Container -->
@endsection
