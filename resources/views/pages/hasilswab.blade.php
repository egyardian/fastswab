@extends('layouts.index')
@section('title','Hasil Swab - Daftar Pasien')
@section('content')

@push('after-style')
<!-- Data Table CSS -->
<link href="{{asset('assets/vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
<!-- Container -->
<div class="container-fluid mt-4">
	<!-- Title -->
	<div class="hk-pg-header align-items-top">
		<div class="px-50 py-19">
            <h2 class="hk-pg-title font-weight-600 mb-10">Hasil Swab - Daftar Pasien {{$lab->nama_lab}}</h2>
            <small>{{$lab->no_lab}}</small>
            <small>Jika berwarna merah berarti positif atau reaktif</small>
		</div>
	</div>
	<!-- /Title -->

	<!-- Row -->
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
                <div class="card-body">
                    {{-- <a href="{{route('lab.create')}}" class="btn btn-primary mb-2">Create Lab</a> --}}
                    <div class="table-responsive">
                        <table class="table table-bordered" id="datable_1">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Pasien</th>
                                    <th>Tanggal Daftar</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<!-- /Row -->
</div>

<div class="modal fade" id="bayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Bayar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('admin.transactionpay')}}" method="POST">
            @csrf
            <div class="modal-body">
                <p class="text-center">Tandai sudah bayar dan buat invoice?</p>
                <input type="hidden" id="idtrans" name="idtrans">
            </div>
            <div class="modal-footer justify-content-between">
                <button type="submit" class="btn btn-success">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="pindah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('hasil.pindah')}}" method="POST">
            @csrf
            <div class="modal-body">
                <p class="text-center">Pindah Data Ke Lab?</p>
                <input type="hidden" id="idtrans" name="idtrans">
                <select name="lab" id="" class="form-control">
                    @foreach ($daftarlab as $item)
                    <option value="{{$item->id}}">{{$item->nama_lab}}</option>

                    @endforeach
                </select>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="submit" class="btn btn-danger">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
    </div>
  </div>

@push('after-scripts')
    <!-- Data Table JavaScript -->
    <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#datable_1').DataTable({
                responsive: false,
                autoWidth: false,
                language: { search: "",
                searchPlaceholder: "Search",
                sLengthMenu: "_MENU_items"

                },
                processing:true,
                serverSide:true,
                ordering:true,
                ajax:{
                    url:'{!! url()->current() !!}',
                },
                columns:[
                    { data:'code',name:'code'},
                    { data:'nama',name:'nama'},
                    { data:'email',name:'email'},
                    { data:'phone',name:'phone'},
                    {
                        data:'pasien',
                        name:'pasien',
                        orderable:'false',
                        searchable: 'false',
                    },
                    {
                        data:'tanggal',
                        name:'tanggal',
                        orderable:'false',
                        searchable: 'false',
                    },
                    {
                        data:'action',
                        name:'action',
                        orderable:'false',
                        searchable: 'false',
                    },

                ]
            });
    });
    </script>
    <script>
        $('#bayar').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var idtrans = button.data('id')
            var modal = $(this)
            modal.find('.modal-body #idtrans').val(idtrans);
        })
        $('#pindah').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var idtrans = button.data('id')
            var modal = $(this)
            modal.find('.modal-body #idtrans').val(idtrans);
        })
    </script>

@if ($message = Session::get('success'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-primary',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@if ($message = Session::get('error'))
    <script>
         $(document).ready(function() {
        $.toast({
		heading: 'Well done!',
		text: '<p>{{$message}}</p>',
		position: 'top-right',
		loaderBg:'#00acf0',
		class: 'jq-toast-danger',
		hideAfter: 3500,
		stack: 6,
		showHideTransition: 'fade'
    });
});
    </script>


@endif
@endpush
<!-- /Container -->
@endsection
