<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>FastSwab</title>

<style type="text/css">

    table{
        font-size: 14px;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: 14px
    }
    .page-break {
    page-break-after: always;
}
 /**
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
             @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 3cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
            }

            /** Define the header rules **/
            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 4cm;
            }

            /** Define the footer rules **/
            footer {
                position: fixed;
                bottom: 0cm;
                left: 0cm;
                right: 0cm;
                height: 3.5cm;
            }
</style>

</head>
<body>
@foreach ($hasil as $item)

<header>
    <img src="https://fastswab.id/{{$item->transaction->lab->header_template}}" width="100%" height="100%"/>
</header>


  <br/>

  <table width="100%">
    <tr>
        <td align="right"><strong>{{$lab->kota}}, {{date('d-m-Y',strtotime($item->tanggal_test))}}</strong></td>
    </tr>
  </table>
  <br>
  <br>

  <table width="100%">
    <tr>
        <td>Yang bertanda tangan dibawah ini menerangkan bahwa :</td>
    </tr>
  </table>
  <br>

  <table width="100%">
    <tr>
        @php
             $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
             $bulan = $array_bulan[date('n')];
        @endphp
        <td width="170">Barcode</td>
        <td>: {{$lab->no_lab}}/{{ $bulan }}/{{ date('Y') }}/{{ $item->pasien->id }}</td>
    </tr>
    <tr>
        <td width="170">NIK</td>
        <td>: {{$item->pasien->nik}}</td>
    </tr>
    <tr>
        <td width="170">Nama</td>
        <td>: {{$item->pasien->nama}}</td>
    </tr>
    <tr>
        <td width="170">Jenis Kelamin</td>
        <td>: {{$item->pasien->jenis_kelamin}}</td>
    </tr>
    <tr>
        <td width="170">Tanggal Lahir</td>
        <td>: {{date('d/m/Y',strtotime($item->pasien->tanggal_lahir))}}</td>
    </tr>
    <tr>
        <td width="170">Tempat Lahir</td>
        <td>: {{$item->pasien->tempat_lahir}}</td>
    </tr>
    <tr>
        <td width="170">Umur</td>
        <td>: {{$item->pasien->umur}}</td>
    </tr>
    <tr>
        <td width="170">No Hp</td>
        <td>: {{$transactions->phone}}</td>
    </tr>
    <tr>
        <td width="170">Alamat</td>
        <td>: {{$item->pasien->alamat}}</td>
    </tr>
  </table>
<br>
  <table width="100%">
    <tr>
        <td width="170">Tempat Pengambilan Spesimen</td>
        <td>: {{$lab->nama_lab}}</td>
    </tr>
  </table>
  <br>
  <table width="100%">
    <tr>
        <td width="170"><strong>Hasil {{$item->name_test}}</strong></td>
        <td>: <strong>{{$item->hasil_test}}</strong></td>
    </tr>
    <tr>
        <td width="170">Suhu Badan</td>
        <td>: {{$item->suhu_badan}}</td>
    </tr>
    <tr>
        <td width="170">Tanggal Test</td>
        <td>: {{\Carbon\Carbon::parse($item->updated_at)->format('d/m/Y H:i:s')}}</td>
    </tr>
    <tr>
        <td width="170">No. Lab</td>
        <td>: {{$lab->no_lab}}</td>
    </tr>
    <tr>
        <td width="170">Laboratorium Pemeriksaan</td>
        <td>: {{$lab->nama_lab}}</td>
    </tr>
  </table>
  <h5 style="text-decoration: underline;" align="center"><strong>BERLAKU {{$item->expired}}X24JAM</strong></h5>
  <table width="100%">
    <tr>
        <td>Scan untuk memeriksa keaslian hasil tes</td>
        <td align="right">Penanggung Jawab {{$lab->nama_lab}},</td>
    </tr>
  </table>
  <table width="100%">
    <tr>
        <td>
            <img src="data:image/png;base64, {!! $qrcode !!}">
        </td>
        <td align="right">
            <img src="https://fastswab.id/{{$lab->tanda_tangan}}" alt="" width="170"/>
        </td>
    </tr>
  </table>
  <table width="100%">
    <tr>
        <td style="text-decoration:underline; color:#1475a0" align="right"><strong>{{$lab->pengawas}}</strong></td>
    </tr>
  </table>
<br>
<table width="100%">
    <tr>
        <td>Disclaimer :</td>
    </tr>
  </table>
<table width="100%">
    <tr>
        <td>*Hasil {{$item->name_test}} NEGATIF tidak selalu berarti pasien tidak terinfeksi oleh patogen, namun hanya menunjukan bahwa
            material genetik patogen tidak ditemukan di dalam sampel.</td>
    </tr>
  </table>
<table width="100%">
    <tr>
        <td>*Surat ini berlaku {{$item->expired}}x24 jam di daerah tertentu sesuai dengan ketentuan pemerintah setempat</td>
    </tr>
  </table>
<footer>
    <img src="https://fastswab.id/assets/images/footer.png" width="100%" height="100%"/>
</footer>
  @if($item->pasien->fotoktp != null || $item->pasien->fotoktp !="")
  <div class="page-break"></div>
  <img style="margin-top: 50px;" src="https://fastswab.id/{{$item->pasien->fotoktp}}" width="400" alt="">
  @endif
  @if(!$loop->last)
  <div class="page-break"></div>
  @endif
  @endforeach
</body>
</html>
