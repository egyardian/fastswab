<!DOCTYPE html>
<!--
Template Name: Pangong - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: support@hencework.com
License: You must have a valid license purchased only from themeforest to legally use the template for your project.
-->
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1.0, user-scalable=no" />
    <title>FastSwab I Invoice</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
     <style>
         .hk-invoice-wrap .invoice-from-wrap > .row div:last-child,
            .hk-invoice-wrap .invoice-to-wrap > .row div:last-child {
            text-align: right; }

            @media (max-width: 767px) {
            .hk-invoice-wrap .invoice-from-wrap > .row div:last-child,
            .hk-invoice-wrap .invoice-to-wrap > .row div:last-child {
                text-align: left; } }

            .mb-10 {
                margin-bottom: 20px !important;
            }
            .mb-10 {
                margin-bottom: 10px !important;
            }
            .pa-35 {
                /* padding: 20px !important; */
            }
            .mb-35 {
                margin-bottom: 35px !important;
            }
            .mb-30 {
                margin-bottom: 30px !important;
            }
            .pl-10 {
                padding-left: 10px !important;
            }
            .pb-20{
                padding-bottom: 20px !important;
            }
            .font-13 {
                font-size: 13px !important;
            }
            .font-14 {
                font-size: 14px !important;
            }
            .font-18 {
                font-size: 18px !important;
            }
            .mt-20{
                margin-bottom: 30px !important;
            }
            .w-70 {
                width: 70% !important;
            }
            .py-60 {
                padding-top: 30px !important;
                /* padding-bottom: 30px !important; */
            }
            .text-right{
                text-align: right;
            }
            .d-block{
                display: block;
            }
            .float-right{
                float: right;
            }
            .absolute{
                position: absolute;
            }


     </style>

</head>

<body>
    <section class="hk-sec-wrapper hk-invoice-wrap pa-35">
        <div class="invoice-from-wrap">
            <div class="row">
                <div class="col-xs-7">
                    <img class="img-fluid invoice-brand-img d-block mb-10" src="https://goantigen.com/assets/images/assets/fastswab.png" alt="brand" width="100"  />
                    {{-- <h6 class="mb-5">QuickSwab</h6> --}}
                    <address>
                        <span class="d-block">fastswab@gmail.com</span>
                        <span class="d-block">Hotline: 081234502205</span>
                    </address>
                </div>
                <div class="col-xs-5">
                    <h4 class="mb-35 font-weight-600">Invoice</h4>
                    <span class="d-block">Tanggal: <span class="pl-10 text-dark">{{date('d-m-Y H:i:s')}}</span></span>
                    <span class="d-block">Invoice #<span class="pl-10 text-dark">{{$transactions->code}}</span></span>
                </div>
            </div>
        </div>
        <hr class="mt-0" style="margin-top: 0px !important; margin-bottom: 2px !important;">
        <div class="invoice-to-wrap">
            <div class="row">
                <div class="col-xs-12">
                    <span class="d-block text-uppercase mb-5 font-13">TO:</span>
                    <address>
                        <span class="d-block">Nama: {{$transactions->nama}}</span>
                        <span class="d-block">Email: {{$transactions->email}}</span>
                        <span class="d-block">No Hp: {{$transactions->phone}}</span>
                    </address>
                </div>
            </div>
        </div>
        <div class="invoice-details">
            <div class="table-wrap">
                <div class="table-responsive">
                    <table class="table table-striped table-border mb-0">
                        <thead>
                            <tr>
                                <th class="w-70">Test</th>
                                <th class="text-right">Jumlah(test)</th>
                                <th class="text-right">Harga</th>
                                <th class="text-right">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="w-70">{{$transactions->detail->first()->name_test}}</td>
                                <td class="text-right">{{count($transactions->detail)}}</td>
                                <td class="text-right">Rp.{{number_format($transactions->price_test)}}</td>
                                <td class="text-right">Rp.{{number_format($transactions->amount)}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <small>Note: {!!$transactions->note!!}</small>
            <div class="invoice-sign-wrap text-right py-60">
                {{-- <img class="img-fluid d-inline-block" src="https://quickswab.id/assets/dist/img/signature.png" alt="sign" /> --}}
                <span class="d-block text-light font-14">Admin Approval</span>
            </div>
        </div>
    </section>




</body>

</html>
