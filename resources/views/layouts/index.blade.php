<!DOCTYPE html>
<!--
Template Name: Pangong - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: support@hencework.com
License: You must have a valid license purchased only from themeforest to legally use the template for your project.
-->
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>@yield('title')</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />
     <!-- Favicons -->

  <link rel="icon" href="{{asset('assets/images/logofs.png')}}" type="image/x-icon">
    @stack('before-style')
    @include('includes.style')
    @stack('after-style')
</head>

<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->

	<!-- HK Wrapper -->
	<div class="hk-wrapper hk-vertical-nav">

        @include('includes.topnavbar')

        @include('includes.verticalnav')

        <!-- Main Content -->
        <div class="hk-pg-wrapper">

            @yield('content')

        @include('includes.footer')
        </div>
        <!-- /Main Content -->

    </div>
    <!-- /HK Wrapper -->

    @stack('before-scripts')
    @include('includes.scripts')
    @stack('after-scripts')

</body>

</html>
