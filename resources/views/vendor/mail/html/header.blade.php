<tr>
<td class="header">
<a href="https://fastswab.id" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="{{asset('assets/images/fastswab.png')}}" class="logo" alt="Antigen Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
