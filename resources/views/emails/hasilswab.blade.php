@component('mail::message')
# Hasil Swab Sudah Keluar!

Selamat Hasil Swab anda telah keluar silahkan download dengan cara klik tombol. Terima kasih!

@component('mail::button', ['url' => route('result',[$transactions->code])])
Button Text
@endcomponent

Terima kasih,<br>
Team FastSwab
@endcomponent
