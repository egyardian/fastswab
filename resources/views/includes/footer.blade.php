<!-- Footer -->
<div class="hk-footer-wrap container">
    <footer class="footer">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <p>Powered by<a href="#" class="text-dark" target="_blank">FastSwab</a> © 2021</p>
            </div>
        </div>
    </footer>
</div>
<!-- /Footer -->
