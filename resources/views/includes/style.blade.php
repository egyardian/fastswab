<!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/fastswab.png')}}">
    <link rel="icon" href="{{asset('assets/images/fastswab.png')}}" type="image/x-icon">

	<!-- vector map CSS -->
    <link href="{{asset('assets/vendors/vectormap/jquery-jvectormap-2.0.3.css')}}" rel="stylesheet" type="text/css" />

    <!-- Toggles CSS -->
    <link href="{{asset('assets/vendors/jquery-toggles/css/toggles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendors/jquery-toggles/css/themes/toggles-light.css')}}" rel="stylesheet" type="text/css">

	<!-- Toastr CSS -->
    <link href="{{asset('assets/vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="{{asset('assets/dist/css/style.css')}}" rel="stylesheet" type="text/css">
