<!-- Vertical Nav -->
<nav class="hk-nav hk-nav-dark">
    <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-column">
                <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                    <a class="nav-link " href="{{route('home')}}">
                        <span class="feather-icon"><i data-feather="pie-chart"></i></span>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                @if(Auth::user()->role==1 ||Auth::user()->role==4)
                <li class="nav-item {{ Request::is('dashboard/transactions*') ? 'active' : '' }}">
                    <a class="nav-link " href="{{route('admin.transactions')}}">
                        <span class="feather-icon"><i data-feather="dollar-sign"></i></span>
                        <span class="nav-link-text">Transaction</span>
                    </a>
                </li>
                <li class="nav-item {{ Request::is('dashboard/hasilswab*') ? 'active' : '' }}">
                    <a class="nav-link " href="{{route('hasil.pilihlab')}}">
                        <span class="feather-icon"><i data-feather="printer"></i></span>
                        <span class="nav-link-text">Hasil Swab</span>
                    </a>
                </li>
                <li class="nav-item {{ Request::is('dashboard/swab*') ? 'active' : '' }}">
                    <a class="nav-link " href="{{route('dokter.pilihlab')}}">
                        <span class="feather-icon"><i data-feather="edit-3"></i></span>
                        <span class="nav-link-text">SwabTest</span>
                    </a>
                </li>
                <li class="nav-item {{ Request::is('dashboard/report*') ? 'active' : '' }}">
                    <a class="nav-link " href="{{ route('report') }}">
                        <span class="feather-icon"><i data-feather="book"></i></span>
                        <span class="nav-link-text">Report</span>
                    </a>
                </li>
                <li class="nav-item {{ Request::is('dashboard/finance*') ? 'active' : '' }}">
                    <a class="nav-link " href="{{ route('finance') }}">
                        <span class="feather-icon"><i data-feather="book"></i></span>
                        <span class="nav-link-text">Report Finance</span>
                    </a>
                </li>
                <li class="nav-item {{ Request::is('dashboard/settings*') ? 'active' : '' }}">
                    <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#pengaturan">
                        <span class="feather-icon"><i data-feather="server"></i></span>
                        <span class="nav-link-text">Pengaturan</span>
                    </a>
                    <ul id="pengaturan" class="nav flex-column collapse collapse-level-1">
                        @if(Auth::user()->role==1)
                        <li class="nav-item {{ Request::is('dashboard/user*') ? 'active' : '' }}">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('user.index')}}">User</a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <li class="nav-item {{ Request::is('dashboard/lab*') ? 'active' : '' }}">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('lab.index')}}">Lab</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item {{ Request::is('dashboard/settings/jenistest*') ? 'active' : '' }}">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('jenistest')}}">Jenis Test</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item {{ Request::is('dashboard/settings/addon*') ? 'active' : '' }}">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{route('addon')}}">Addon</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                @endif
                @if(Auth::user()->role==2)
                <li class="nav-item {{ Request::is('dashboard/transactions*') ? 'active' : '' }}">
                    <a class="nav-link " href="{{route('admin.transactionlab',Auth::user()->lab)}}">
                        <span class="feather-icon"><i data-feather="dollar-sign"></i></span>
                        <span class="nav-link-text">Transaction</span>
                    </a>
                </li>
                <li class="nav-item {{ Request::is('dashboard/hasilswab*') ? 'active' : '' }}">
                    <a class="nav-link " href="{{route('hasil.datapasien',Auth::user()->lab)}}">
                        <span class="feather-icon"><i data-feather="printer"></i></span>
                        <span class="nav-link-text">Hasil Swab</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->role==3)
                <li class="nav-item {{ Request::is('dashboard/swab*') ? 'active' : '' }}">
                    <a class="nav-link " href="{{route('dokter.datapasien',Auth::user()->lab)}}">
                        <span class="feather-icon"><i data-feather="edit-3"></i></span>
                        <span class="nav-link-text">SwabTest</span>
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
<!-- /Vertical Nav -->
