    <!-- jQuery -->
    <script src="{{asset('assets/vendors/jquery/dist/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('assets/vendors/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Slimscroll JavaScript -->
    <script src="{{asset('assets/dist/js/jquery.slimscroll.js')}}"></script>

    <!-- Fancy Dropdown JS -->
    <script src="{{asset('assets/dist/js/dropdown-bootstrap-extended.js')}}"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="{{asset('assets/dist/js/feather.min.js')}}"></script>

    <!-- Toggles JavaScript -->
    <script src="{{asset('assets/vendors/jquery-toggles/toggles.min.js')}}"></script>
    <script src="{{asset('assets/dist/js/toggle-data.js')}}"></script>  

	<!-- Counter Animation JavaScript -->
	<script src="{{asset('assets/vendors/waypoints/lib/jquery.waypoints.min.js')}}"></script>
	<script src="{{asset('assets/vendors/jquery.counterup/jquery.counterup.min.js')}}"></script>

	<!-- EChartJS JavaScript -->
    <script src="{{asset('assets/vendors/echarts/dist/echarts-en.min.js')}}"></script>

	<!-- Sparkline JavaScript -->
    <script src="{{asset('assets/vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script>

	<!-- Vector Maps JavaScript -->
    <script src="{{asset('assets/vendors/vectormap/jquery-jvectormap-2.0.3.min.js')}}"></script>
    <script src="{{asset('assets/vendors/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('assets/dist/js/vectormap-data.js')}}"></script>

	<!-- Owl JavaScript -->
    <script src="{{asset('assets/vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

	<!-- Toastr JS -->
    <script src="{{asset('assets/vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>

    <!-- Init JavaScript -->
    <script src="{{asset('assets/dist/js/init.js')}}"></script>
	<script src="{{asset('assets/dist/js/dashboard-data.js')}}"></script>
