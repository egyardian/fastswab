<?php

use App\Mail\HasilSwab;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);
              Route::get('/', 'LandingController@landing')->name('landing');
Route::get('/getkota/{id}', 'LocationController@getkota')->name('getkota');
Route::get('/getkecamatan/{id}', 'LocationController@getkecamatan')->name('getkecamatan');

//Pendaftaran
Route::get('/pendaftaran/{id}','PendaftaranController@pendaftaran')->name('pendaftaran');
Route::get('/daftar/{code}','PendaftaranController@pendaftaranqrcode')->name('pendaftaranqrcode');
Route::post('/daftar','PendaftaranController@daftar')->name('daftar');
Route::get('/hasil/{code}','HasilController@hasilemail')->name('result');

Route::get('/contohpdf','HasilController@testpdf');





Route::prefix('dashboard')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::middleware(['auth', 'superadmin'])->group(function () {

        Route::resource('settings/user', 'UserController');
        Route::resource('settings/lab', 'LabController');
        Route::post('settings/user/changepassword/{id}','UserController@changepassword')->name('super.changepassword');
        Route::get('settings/lab/downloadqrcode/{no_lab}','LabController@downloadqrcode')->name('super.downloadqrcode');

        Route::get('settings/jenistest','PengaturanController@JenisTest')->name('jenistest');
        Route::get('settings/jenistest/create','PengaturanController@addviewJenisTest')->name('addviewjenistest');
        Route::post('settings/jenistest/add','PengaturanController@addJenisTest')->name('addjenistest');
        Route::get('settings/jenistest/edit/{id}','PengaturanController@editJenistest')->name('editjenistest');
        Route::post('settings/jenistest/update/{id}','PengaturanController@updateJenisTest')->name('updatejenistest');

        Route::get('settings/addon','PengaturanController@addon')->name('addon');
        Route::get('settings/createviewaddon','PengaturanController@createviewaddon')->name('createviewaddon');
        Route::post('settings/createaddon','PengaturanController@createaddon')->name('createaddon');
        Route::get('settings/addon/edit/{id}','PengaturanController@editaddon')->name('editaddon');
        Route::post('settings/addon/update/{id}','PengaturanController@updateaddon')->name('updateaddon');

        Route::get('report','ReportController@report')->name('report');
        Route::get('printout','ReportController@printout')->name('printout');
        Route::get('hasilreport/{id}','ReportController@hasilsatu')->name('hasilsatu');

        Route::get('/finance','ReportFinanceController@index')->name('finance');
        Route::get('/finance/filter','ReportFinanceController@filter')->name('financeFilter');
        Route::get('/finance/export','ReportFinanceController@export')->name('financeExport');

     });
     Route::middleware(['auth', 'admin'])->group(function () {

         Route::get('/transactions','TransactionController@transactions')->name('admin.transactions');
         Route::get('/transactions/{id}','TransactionController@transactionlab')->name('admin.transactionlab');
         Route::get('/transactions/reorder/{transaction_code}/{lab_id}','TransactionController@reorder')->name('admin.reorder');
         Route::post('/transactions/reordersubmit','TransactionController@reordersubmit')->name('admin.reordersubmit');
         Route::get('/transactions/detail/{code}','TransactionController@transactiondetail')->name('admin.transactiondetail');
         Route::get('/transactions/invoice/{code}','TransactionController@transactioninvoice')->name('admin.transactioninvoice');
         Route::get('/transactions/viewinvoice/{code}','TransactionController@transactionviewinvoice')->name('admin.transactionviewinvoice');
         Route::post('/transactions/pay','TransactionController@transactionpay')->name('admin.transactionpay');
         Route::post('/transactions/delete','TransactionController@transactiondelete')->name('admin.transactiondelete');

         Route::get('/hasilswab','HasilController@pilihlab')->name('hasil.pilihlab');
         Route::get('/hasilswab/{id}','HasilController@datapasien')->name('hasil.datapasien');
         Route::get('/hasilpdf/{id}/{no_lab}','HasilController@hasilpdf')->name('hasil.hasilpdf');

         Route::post('/pindahdata','HasilController@pindahlab')->name('hasil.pindah');
     });
     Route::middleware(['auth', 'dokter'])->group(function () {

         Route::get('/swab','SwabController@pilihlab')->name('dokter.pilihlab');
         Route::get('/swab/{id}','SwabController@datapasien')->name('dokter.datapasien');
         Route::get('/swab/input/{code}','SwabController@swabinput')->name('dokter.swabinput');
         Route::post('/swab/update','SwabController@swabupdate')->name('dokter.swabupdate');
         Route::get('/hasil','SwabController@hasiltest')->name('dokter.hasil');
     });
});
