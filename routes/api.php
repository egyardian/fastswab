<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\AuthController@login');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('logout', 'Api\AuthController@logout');
    Route::post('transaction', 'Api\TransactionController@transactions');
    Route::post('alltransaction', 'Api\TransactionController@alltransaction');
    Route::post('alltransaction/{per_page}', 'Api\TransactionController@alltransactionperpage');
    Route::post('transactionupdate', 'Api\TransactionController@transactionupdate');
    Route::get('statistik', 'Api\TransactionController@statistik');
    Route::post('diagnosa', 'Api\DiagnosaController@diagnosa');
    Route::post('pendaftaran', 'Api\PendaftaranController@pendaftaran');
    Route::get('pendaftaran/{id}','Api\PendaftaranController@getpendaftaran');
    Route::get('getlabs','Api\LabController@labs');

});
