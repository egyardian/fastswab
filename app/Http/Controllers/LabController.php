<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lab;
use QrCode;

class LabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labs = Lab::all();
        return view('pages.lab')->with(array('labs' => $labs));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['provinsi'] = json_decode($this->provinsi())->provinsi;
        return view('pages.labcreate',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'no_lab' => 'required|unique:labs,no_lab',
            'nama_lab' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'alamat' => 'required',
            'pengawas' => 'required',
            'tanda_tangan' => 'required|image',
            'header_template' => 'required|image',
         ]);
        $lab = new Lab;
        $lab->no_lab = $request->no_lab;
        $lab->nama_lab = $request->nama_lab;
        $lab->provinsi = $request->provinsi;
        $lab->kota = $request->kota;
        $lab->kecamatan = $request->kecamatan;
        $lab->alamat = $request->alamat;
        $lab->type = $request->type;
        $lab->pengawas = $request->pengawas;
        $file = $request->tanda_tangan;
        $filename = $request->no_lab.'.'.$file->getClientOriginalExtension();
        $path = "assets/images/";
        $file->move(public_path().'/'.$path, $filename);
        $lab->tanda_tangan = $path.$filename;
        $header = $request->header_template;
        $filename = rand().$request->no_lab.'.'.$header->getClientOriginalExtension();
        $path = "assets/images/";
        $header->move(public_path().'/'.$path, $filename);
        $lab->header_template = $path.$filename;
        $lab->save();

        return redirect()->route('lab.index')->with(['success' => 'Create Lab Succesfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['lab'] = Lab::findOrFail($id);
        $data['provinsi'] = json_decode($this->provinsi())->provinsi;

        // dd($data['provinsi']->provinsi);

        return view('pages.labedit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'no_lab' => 'required',
            'nama_lab' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
            'kecamatan' => 'required',
            'alamat' => 'required',
            'pengawas' => 'required',
            'tanda_tangan' => 'image',
            'header_template' => 'image',
         ]);
        $lab = Lab::findOrFail($id);
        $lab->no_lab = $request->no_lab;
        $lab->nama_lab = $request->nama_lab;
        $lab->provinsi = $request->provinsi;
        $lab->kota = $request->kota;
        $lab->kecamatan = $request->kecamatan;
        $lab->alamat = $request->alamat;
        $lab->pengawas = $request->pengawas;
        $lab->type = $request->type;
        if($request->tanda_tangan){
            $file = $request->tanda_tangan;
            $filename = time().$request->no_lab.'.'.$file->getClientOriginalExtension();
            $path = "assets/images/";
            $file->move(public_path().'/'.$path, $filename);
            $lab->tanda_tangan = $path.$filename;
        }
        if($request->header_template){
            $header = $request->header_template;
            $filename = rand().$request->no_lab.'.'.$header->getClientOriginalExtension();
            $path = "assets/images/";
            $header->move(public_path().'/'.$path, $filename);
            $lab->header_template = $path.$filename;
        }
        $lab->save();

        return redirect()->route('lab.index')->with(['success' => 'Update Lab Succesfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = Lab::findOrFail($request->idlab);
        $user->delete();
        return back()->with(['error' => 'Delete Lab Success']);
    }

    public function downloadqrcode($no_lab)
    {
        $url = route('pendaftaran', ['id' => $no_lab]);
        QrCode::size(1920)
            ->format('png')
            ->generate($url, public_path("assets/images/qrcode/".$no_lab.".png"));

            return response()->download(public_path("assets/images/qrcode/".$no_lab.".png"));
    }
}
