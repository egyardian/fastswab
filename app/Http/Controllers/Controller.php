<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function provinsi()
    {

        $response = Http::get('http://dev.farizdotid.com/api/daerahindonesia/provinsi');

        return $response;

    }

    public function kota($id)
    {
        $response = Http::get('http://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi='.$id);

        return $response;
    }
    public function kecamatan($id)
    {
        $response = Http::get('http://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota='.$id);

        return $response;
    }
}
