<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Lab;
use App\DetailTransaction;
use App\Mail\HasilSwab;
use PDF;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;

class SwabController extends Controller
{
    public function pilihlab()
    {
        $labs = Lab::all();
        return view('pages.swabpilihlab')->with(array('labs' => $labs));
    }
    public function datapasien($id)
    {
        $lab = Lab::where('id',$id)->first();
        if(request()->ajax())
        {
            $transactions = Transaction::where('lab_id',$lab->id)->where('status',1)->with('detail')->orderBy('created_at','desc');
            return Datatables::of($transactions)
                ->addColumn('action',function($item) {
                        return '
                            <td>
                                <a href="'.route('dokter.swabinput',$item->code).'" class="btn btn-primary px-2"><i data-feather="edit-3"></i>Input Hasil Swab</a>
                            </td>
                        ';

                })
                ->addColumn('test',function($item){
                    if($item->detail->count() > 0){
                        return $item->detail->first()->name_test;
                    }else{
                        return 'Undefined';
                    }
                })
                ->addColumn('pasien',function($item){
                    if($item->detail){
                        return count($item->detail);
                    }else{
                        return '0';
                    }
                })
                ->rawColumns(['action','test','pasien'])
                ->make()
                ;

        }

        return view('pages.swab')->with(array(
                                                        'lab' => $lab));
    }

    public function swabinput($code)
    {
        $transactions = Transaction::where('code',$code)->with('detail.pasien')->first();
        $lab = Lab::where('id',$transactions->lab_id)->first();
        // dd($transactions);
        return view('pages.swabinput')->with(array('transactions' => $transactions, 'lab'=>$lab));
    }

    public function swabupdate(Request $request)
    {
        for ($i=0; $i < count($request->hasil_test) ; $i++) {
            $hasiltest = DetailTransaction::findOrFail($request->id_detail[$i]);
            $hasiltest->suhu_badan = $request->suhu_badan[$i];
            $hasiltest->hasil_test = $request->hasil_test[$i];
            $hasiltest->save();

        }
        $transactions = Transaction::where('code',$request->code)->first();
        Mail::to($transactions->email)->send(new HasilSwab($transactions));
        return back()->with(['success' => 'Berhasil Input Hasil Swab dan mengirim email ke pasien']);
    }

    public function hasiltest()
    {
        $pdf = PDF::loadView('pdf.template');
        return $pdf->stream("invoice.pdf");
    }
}
