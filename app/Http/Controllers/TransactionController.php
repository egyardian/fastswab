<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Lab;
use PDF;
use App\Pasien;
use App\DetailTransaction;
use App\JenisTest;
use App\Addon;
use Yajra\Datatables\Datatables;
use Auth;


class TransactionController extends Controller
{
    public function transactions()
    {
        $labs = Lab::all();
        return view('pages.transactions')->with(array('labs' => $labs));

    }

    public function transactionlab($id)
    {
        $addons = Addon::where('status',1)->get();
        $lab = Lab::where('id',$id)->first();
        if(request()->ajax())
        {
            $transactions = Transaction::where('lab_id',$lab->id)->with('detail')->orderBy('created_at','desc');
            return Datatables::of($transactions)
                ->addColumn('action',function($item) {
                    if($item->status == 1){
                        if(Auth::user()->role == 1){
                            return '
                                <a href="'.route('admin.transactiondetail',$item->code).'" class="btn btn-primary btn-sm px-2"><i class="fa fa-eye"></i></a>
                                                <button type="button" class="btn btn-success btn-sm px-2 d-inline" data-id="'.$item->id.'" disabled data-toggle="modal" data-target="#bayar"><i class="fa fa-money"></i> Bayar</button>
                                                <button type="button" class="btn btn-danger btn-sm px-2 d-inline" data-id="'.$item->id.'" data-toggle="modal" data-target="#hapus"><i class="fa fa-trash"></i></button>
                                                <a class="btn btn-warning btn-sm" href="'.route('admin.reorder',[$item->code,$item->lab_id]).'">reorder</a>
                            ';

                        }else{
                            return '
                                <a href="'.route('admin.transactiondetail',$item->code).'" class="btn btn-primary btn-sm px-2"><i class="fa fa-eye"></i></a>
                                                <button type="button" class="btn btn-success btn-sm px-2 d-inline" data-id="'.$item->id.'" disabled data-toggle="modal" data-target="#bayar"><i class="fa fa-money"></i> Bayar</button>
                                                <a class="btn btn-warning btn-sm" href="'.route('admin.reorder',[$item->code,$item->lab_id]).'">reorder</a>
                            ';
                        }

                    }else{
                        if(Auth::user()->role == 1){
                            return '
                                <a href="'.route('admin.transactiondetail',$item->code).'" class="btn btn-primary btn-sm px-2"><i class="fa fa-eye"></i></a>
                                                <button type="button" class="btn btn-success btn-sm px-2 d-inline" data-id="'.$item->id.'" data-toggle="modal" data-target="#bayar"><i class="fa fa-money"></i> Bayar</button>
                                                <button type="button" class="btn btn-danger btn-sm px-2 d-inline" data-id="'.$item->id.'" data-toggle="modal" data-target="#hapus"><i class="fa fa-trash"></i></button>
                                                <a class="btn btn-warning btn-sm" href="'.route('admin.reorder',[$item->code,$item->lab_id]).'">reorder</a>
                            ';

                        }else{
                            return '
                                <a href="'.route('admin.transactiondetail',$item->code).'" class="btn btn-primary btn-sm px-2"><i class="fa fa-eye"></i></a>
                                                <button type="button" class="btn btn-success btn-sm px-2 d-inline" data-id="'.$item->id.'" data-toggle="modal" data-target="#bayar"><i class="fa fa-money"></i> Bayar</button>
                                                <button type="button" class="btn btn-danger btn-sm px-2 d-inline" data-id="'.$item->id.'" data-toggle="modal" data-target="#hapus"><i class="fa fa-trash"></i></button>
                                                <a class="btn btn-warning btn-sm" href="'.route('admin.reorder',[$item->code,$item->lab_id]).'">reorder</a>
                            ';
                        }
                    }
                })
                ->editColumn('status',function($item){
                    if($item->status == 1){
                        return '<span class="badge badge-success">Paid</span>';
                    }else{
                        return '<span class="badge badge-danger">Not Paid</span>';

                    }
                })
                ->addColumn('pasien',function($item){
                    if($item->detail){
                        return count($item->detail);
                    }else{
                        return '0';
                    }
                })
                ->rawColumns(['action','pasien','status'])
                ->make()
                ;

        }

        // dd($transactions);
        return view('pages.transactionlab')->with(array(
                                                        'lab' => $lab,
                                                        'addons' => $addons,));
    }

    public function transactiondetail($code)
    {
        $transactions = Transaction::where('code',$code)->with('detail.pasien')->first();
        $lab = Lab::where('id',$transactions->lab_id)->first();
        // dd($transactions);
        return view('pages.transactiondetail')->with(array('transactions' => $transactions, 'lab'=>$lab));
    }

    public function transactionviewinvoice($code)
    {
        $transactions = Transaction::where('code',$code)->with('detail.pasien')->first();
        // dd($transactions);

        return view('pages.transactioninvoice')->with(array('transactions' => $transactions));
    }

    public function transactioninvoice($code)
    {
        $transactions = Transaction::where('code',$code)->with('detail.pasien')->first();

        $pdf = PDF::loadView('pdf.invoice',compact('transactions'))->setPaper('a5', 'landscape');
        return $pdf->stream("invoice-".$code.".pdf", array("Attachment" => false));


        // return view('pdf.invoice');
    }

    public function transactionpay(Request $request)
    {
        $transactions = Transaction::findOrFail($request->idtrans);
        $transactions->status = 1;
        if($request->addon){
            $addon = Addon::where('id',$request->addon)->first();
            $totaladdon = $addon->price*$request->qty;
            $transactions->amount = $transactions->amount + $totaladdon;
            $transactions->note = $addon->name." - Rp".number_format($addon->price)."x".$request->qty;
        }
        $transactions->save();

        return redirect()->route('admin.transactionviewinvoice',[$transactions->code]);
    }

    public function transactiondelete(Request $request)
    {
        Transaction::findOrFail($request->idtrans)->delete();
        $pasien_id = DetailTransaction::where('transaction_id',$request->idtrans)->pluck('pasien_id')->toArray();
        DetailTransaction::where('transaction_id',$request->idtrans)->delete();
        Pasien::whereIn('id',$pasien_id)->delete();

        return back()->with(['error' => 'Delete Data Success']);
    }

    public function deletedetail()
    {
        $transactions = Transaction::pluck('id')->toArray();

        $pasien_id = DetailTransaction::whereNotIn('transaction_id',$transactions)->pluck('pasien_id')->toArray();
        // echo $transactions;
        DetailTransaction::whereNotIn('transaction_id',$transactions)->delete();
        Pasien::whereIn('id',$pasien_id)->delete();

        return "berhasil";
    }

    public function reorder($transaction_code,$id_lab)
    {
        $lab = Lab::where('id',$id_lab)->first();
        $jenistests = JenisTest::where('status',1)->get();
        $addons = Addon::where('status',1)->get();


        $transaction = Transaction::where('code',$transaction_code)->with('detail.pasien')->first();

        // dd($transaction);

        return view('pages.transactionreorder',compact('lab','jenistests','transaction','addons'));
    }

    public function reordersubmit(Request $request)
    {
        $hargaaddon = 0;
        $note="";
        if($request->addon){
            $addon = Addon::whereIn('id',$request->addon)->get();

            foreach($addon as $item){
                $hargaaddon= $hargaaddon+$item->price;
                $note .= $item->name.' - Rp '.number_format($item->price).'<br>';
            }

        }
        $jenistest = JenisTest::findOrFail($request->jenistest_id);
        $transaction = new Transaction;
        $transaction->code = 'FS'.date('YmdHis').$request->no_lab;
        $transaction->lab_id = $request->lab_id;
        $transaction->nama = $request->nama[0];
        $transaction->email = $request->email;
        $transaction->phone = $request->phone;
        $transaction->price_test = $jenistest->price_test;
        $transaction->amount = $jenistest->price_test*count($request->nama)+$hargaaddon;
        $transaction->note = $note;

        $transaction->save();

        for ($i=0; $i < count($request->nama) ; $i++) {
            $pasien = new Pasien;
            $pasien->nik = $request->nik[$i];
            $pasien->nama = $request->nama[$i];
            $pasien->jenis_kelamin = $request->jenis_kelamin[$i];
            $pasien->tanggal_lahir = $request->tanggal_lahir[$i];
            $pasien->tempat_lahir = $request->tempat_lahir[$i];
            $pasien->umur = \Carbon\Carbon::parse($request->tanggal_lahir[$i])->diff(\Carbon\Carbon::now())->format('%y tahun, %m bulan');
            $pasien->alamat = $request->alamat[$i];
            $pasien->fotoktp = $request->fotoktp[$i];


            $pasien->save();

            $detail = new DetailTransaction;
            $detail->transaction_id = $transaction->id;
            $detail->pasien_id = $pasien->id;
            $detail->tanggal_test = date('Y-m-d');
            $detail->suhu_badan = $request->suhu_badan[$i];
            $detail->name_test = $jenistest->name_test;
            $detail->expired = $jenistest->expire_day;
            $detail->save();
        }

        return redirect()->route('admin.transactionlab',[$request->lab_id])->with(['success' => 'Daftar Ulang Berhasil. dan masuk ke daftar transaksi']);
    }
}
