<?php

namespace App\Http\Controllers;

use App\Exports\FinanceExport;
use App\JenisTest;
use App\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ReportFinanceController extends Controller
{
    public function index()
    {
        $d['jenis_test'] = JenisTest::where('status',1)->get();
        return view('pages.reportfinance',$d);
    }

    public function filter(Request $request)
    {
        $jenis = $request->jenis;
        if($request->jenis == 'all'){

            $d['transaction'] = Transaction::whereBetween('updated_at',[$request->start,$request->end])
                                            ->has('detail')
                                            ->where('status',1)
                                            ->get();
        }else{
            $d['transaction'] = Transaction::whereBetween('updated_at',[$request->start,$request->end])
                                            ->whereHas('detail', function (Builder $query) use($jenis) {
                                                $query->where('name_test', $jenis);
                                            })
                                            ->where('status',1)
                                            ->get();
        }

        return view('pages.data_finance',$d)->render();
    }

    public function export(Request $request)
    {
        return Excel::download(new FinanceExport($request->start,$request->end,$request->jenis),'reportfinance.xlsx');
    }
}
