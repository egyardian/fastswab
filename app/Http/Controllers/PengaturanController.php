<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisTest;
use App\Addon;

class PengaturanController extends Controller
{
    public function JenisTest()
    {
        $jenistests = JenisTest::all();
        return view('pages.pengaturan.jenistest',compact('jenistests'));
    }

    public function addviewJenisTest()
    {
        return view('pages.pengaturan.createjenistest');
    }

    public function addJenisTest(Request $request)
    {
        $this->validate($request,[
            'name_test' => 'required',
            'price_test' => 'required|numeric',
            'expire_day' => 'required|numeric',
            'status' => 'required',
         ]);
        $jenistest = new JenisTest;
        $jenistest->name_test = $request->name_test;
        $jenistest->price_test = $request->price_test;
        $jenistest->expire_day = $request->expire_day;
        $jenistest->status = $request->status;
        $jenistest->save();

        return redirect()->route('jenistest')->with(['success' => 'Tambah Jenis Test Berhasil']);
    }

    public function editJenisTest($id)
    {
        $jenistest = JenisTest::findOrFail($id);
        return view('pages.pengaturan.editjenistest',compact('jenistest'));
    }

    public function updateJenisTest(Request $request, $id)
    {
        $this->validate($request,[
            'name_test' => 'required',
            'price_test' => 'required|numeric',
            'expire_day' => 'required|numeric',
            'status' => 'required',
         ]);
        $jenistest = JenisTest::findOrFail($id);
        $jenistest->name_test = $request->name_test;
        $jenistest->price_test = $request->price_test;
        $jenistest->expire_day = $request->expire_day;
        $jenistest->status = $request->status;
        $jenistest->save();

        return redirect()->route('jenistest')->with(['success' => 'Edit Jenis Test Berhasil']);

    }

    public function addon()
    {
        $addons = Addon::all();
        return view('pages.pengaturan.addon',compact('addons'));
    }

    public function createviewaddon()
    {
        return view('pages.pengaturan.createaddon');
    }

    public function createaddon(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'price' => 'required|numeric',
            'status' => 'required',
         ]);
        $jenistest = new Addon;
        $jenistest->name = $request->name;
        $jenistest->price = $request->price;
        $jenistest->status = $request->status;
        $jenistest->save();

        return redirect()->route('addon')->with(['success' => 'Tambah Addon Berhasil']);
    }

    public function editaddon($id)
    {
        $addon = Addon::findOrFail($id);
        return view('pages.pengaturan.editaddon',compact('addon'));
    }

    public function updateaddon(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'price' => 'required|numeric',
            'status' => 'required',
         ]);
        $jenistest = Addon::findOrFail($id);
        $jenistest->name = $request->name;
        $jenistest->price = $request->price;
        $jenistest->status = $request->status;
        $jenistest->save();

        return redirect()->route('addon')->with(['success' => 'Edit Addon Berhasil']);

    }
}
