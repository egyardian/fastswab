<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function getkecamatan($id)
    {
        $kecamatan = $this->kecamatan($id);
        return $kecamatan;
    }
    public function getkota($id)
    {
        $kota = $this->kota($id);
        return $kota;
    }
}
