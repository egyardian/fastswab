<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Lab;

class LabController extends Controller
{
    public function labs()
    {
        $labs = Lab::all();
        return response()->json($labs);
    }
}
