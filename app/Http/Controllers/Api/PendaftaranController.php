<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Lab;
use App\Transaction;
use App\JenisTest;
use App\DetailTransaction;
use App\Pasien;
use Intervention\Image\Facades\Image;

class PendaftaranController extends Controller
{
    public function getpendaftaran($no_lab)
    {
        $lab = Lab::where('id',$no_lab)->first();
        $jenistests = JenisTest::where('status',1)->get();
        if($lab){
            return response()->json([
                                    'success'       => true,
                                    'lab'           => $lab,
                                    'jenistests'    => $jenistests
            ], 200);
        }
    }

    public function pendaftaran(Request $request)
    {
        // dd(count($request->pasien));
        $transaction = new Transaction;
        $transaction->code = 'QS'.date('YmdHis').$request->no_lab;
        $transaction->lab_id = $request->lab_id;
        $transaction->nama = $request->nama;
        $transaction->email = $request->email;
        $transaction->phone = $request->phone;
        $transaction->amount = 235000*count($request->pasien);

        // dd($transaction);
        $transaction->save();

        foreach($request->pasien as $key => $value){
            $pasien = new Pasien;
            $pasien->nik = $value['nik'];
            $pasien->nama = $value['nama'];
            $pasien->jenis_kelamin = $value['jenis_kelamin'];
            $pasien->tanggal_lahir = $value['tanggal_lahir'];
            $pasien->tempat_lahir = $value['tempat_lahir'];
            $pasien->umur = $value['umur'];
            $pasien->alamat = $value['alamat'];
            if($value['fotoktp'])
            {
                $filename = time().rand(0,990).$transaction->id.'.jpg';
                $path = "assets/images/".$filename;
                $fotoktp = Image::make($value['fotoktp'])->resize(300, 200);
                $fotoktp->save($path);
                $pasien->fotoktp = $path;
            }

            $pasien->save();

            $detail = new DetailTransaction;
            $detail->transaction_id = $transaction->id;
            $detail->pasien_id = $pasien->id;
            $detail->tanggal_test = date('Y-m-d');

            $detail->save();
        }
        //     if(isset($request->fotoktp[$i])){
        //         $file = $request->fotoktp[$i];
                // $file->move(public_path().'/'.$path, $filename);
        //     }

        //     $pasien->save();

        // }

        return response()->json(['success' => true,
                                 'code' => $transaction->code,
        ], 200);
    }
}
