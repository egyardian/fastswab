<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DetailTransaction;
use App\Transaction;
use Mail;
use App\Mail\HasilSwab;

class DiagnosaController extends Controller
{
    public function diagnosa(Request $request)
    {
        $this->validate($request,[
            'pasien_id' => 'required',
            'diagnosa' => 'required',
         ]);


         $detail = DetailTransaction::where('pasien_id',$request->pasien_id)->first();
         if($detail)
         {
            $detail->hasil_test = $request->diagnosa;
            $detail->save();
            $jmlhasil = DetailTransaction::where('transaction_id', $detail->transaction_id)->whereNotNull('hasil_test')->count();
            $total = DetailTransaction::where('transaction_id', $detail->transaction_id)->count();
            if($jmlhasil == $total)
            {
                $transactions = Transaction::where('id',$detail->transaction_id)->first();
                Mail::to($transactions->email)->send(new HasilSwab($transactions));
            }
            return response()->json(['success' => 'Success'], 200);
         }
         else{
            return response()->json(['error'=>'Unauthorised'], 401);
         }
    }
}
