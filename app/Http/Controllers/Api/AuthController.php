<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\User;

class AuthController extends Controller
{
    public function login(){
        $checkuser = User::where('username',request('username'))->first();
        if($checkuser){
            if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){
                $user = Auth::user();
                $success['token']   =  $user->createToken('nApp')->accessToken;
                $success['id']      = $user->id;
                $success['name']    = $user->name;
                $success['role']    = $user->role;
                $success['lab_id']     = $user->lab_id;
                $success['no_lab']     = $user->no_lab;

                return response()->json(['success' => $success], 200);
            }
            else{
                return response()->json(['error'=>'Unauthorised'], 401);
            }

        }else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function logout(Request $request)
    {
        $logout = $request->user()->token()->revoke();
        if($logout){
            return response()->json([
                'message' => 'Successfully logged out'
            ]);
        }
    }
}
