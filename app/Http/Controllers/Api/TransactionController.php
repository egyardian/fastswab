<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transaction;
use App\User;
use App\Pasien;
Use App\DetailTransaction;
use Carbon\Carbon;


class TransactionController extends Controller
{
    public function transactions(Request $request)
    {
        $this->validate($request,[
            'code' => 'required',
         ]);

        $transactions = Transaction::where('code',$request->code)->with('detail.pasien')->first();

        return response()->json($transactions);
    }

    public function transactionupdate(Request $request)
    {
        $this->validate($request,[
            'code' => 'required',
         ]);

         $transactions = Transaction::where('code',$request->code)->with('detail.pasien')->first();
         $transactions->status = 1;
         $transactions->save();

        return response()->json($transactions);

    }

    public function alltransactionperpage($per_page)
    {
        $transactions = Transaction::orderBy('created_at','desc')->paginate($per_page);

        return response()->json($transactions);
    }
    public function alltransaction()
    {
        $transactions = Transaction::orderBy('created_at','desc')->paginate();

        return response()->json($transactions);
    }

    public function statistik()
    {
        $user = User::count();
        $pasien = Pasien::count();
        $positif = DetailTransaction::where('hasil_test','POSITIF')->orWhere('hasil_test','Reaktif')->count();
        $negatif = DetailTransaction::where('hasil_test','NEGATIF')->orWhere('hasil_test','Non-Reaktif')->count();
        $total = Transaction::where('status',1)->sum('amount');
        $totalhariini = Transaction::where('status',1)->whereDate('created_at', Carbon::today())->sum('amount');

        return response()->json(['success' => true, 'user' => $user, 'pasien' => $pasien, 'positif' => $positif, 'negatif' => $negatif, 'total' => $total, 'totalhariini' => $totalhariini], 200);
    }


}
