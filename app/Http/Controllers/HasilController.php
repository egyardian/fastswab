<?php

namespace App\Http\Controllers;

use App\DetailTransaction;
use Illuminate\Http\Request;
use App\Lab;
use App\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Yajra\Datatables\Datatables;
use PDF;
use Carbon\Carbon;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
// use App\Mail\HasilSwab;

class HasilController extends Controller
{
    public function pilihlab()
    {
        $labs = Lab::all();
        return view('pages.hasilswabpilihlab')->with(array('labs' => $labs));
    }

    public function datapasien($id)
    {
        $lab = Lab::where('id',$id)->first();

        $daftarlab = Lab::where('id','!=',$id)->get();

        if(request()->ajax())
        {
            $transactions = Transaction::where('lab_id',$lab->id)->orderBy('created_at','desc')->where('status',1)->whereHas('detail', function (Builder $query) {
                $query->whereNotNull('hasil_test');
            })->with('detail');
            return Datatables::of($transactions)
                ->setRowClass(function ($item) {
                    foreach($item->detail as $pasien){
                        if($pasien->hasil_test == "POSITIF" || $pasien->hasil_test == "POSITIF/REAKTIF" || $pasien->hasil_test == "Reaktif" || $pasien->hasil_test == "Reaktif IgM" || $pasien->hasil_test == "Reaktif IgG"){
                            return 'table-danger';
                            break;
                        }
                    }
                })
                ->addColumn('action',function($item) {
                        $phone = preg_replace("/^0/", "62", $item->phone);
                        $jmlhasil = DetailTransaction::where(['transaction_id' => $item->id])->whereNotNull('hasil_test')->count();
                        $lab = Lab::where('id',$item->lab_id)->first();
                        if($jmlhasil == count($item->detail)){
                            return '
                                <td>
                                    <a href="https://api.whatsapp.com/send?phone='.$phone.'&text=Hasil%20Swab%20Antigen%20anda%20telah%20keluar!%20Silahkan%20klik%20link%20dibawah%20ini%20untuk%20mendownload%20hasil.%0A'.route('result',$item->code).'" target="_blank" class="btn btn-success"><i class="fa fa-whatsapp"></i>WA</a>
                                    <a href="'.route('hasil.hasilpdf',[$item->id,$lab->no_lab]).'" target="_blank" class="btn btn-danger px-2"><i data-feather="printer"></i>PDF</a>
                                    <button type="button" class="btn btn-success px-2 d-inline" data-id="'.$item->id.'" data-toggle="modal" data-target="#pindah"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                                </td>
                            ';

                        }else{
                            return '
                                <td>
                                    <a href="https://api.whatsapp.com/send?phone='.$phone.'&text=Hasil%20Swab%20Antigen%20anda%20telah%20keluar!%20Silahkan%20klik%20link%20dibawah%20ini%20untuk%20mendownload%20hasil.%0A'.route('result',$item->code).'" target="_blank" class="btn btn-success"><i class="fa fa-whatsapp"></i>WA</a>
                                    <button class="btn btn-light" disabled>Belum Semua</button>
                                </td>
                            ';
                        }

                })
                ->addColumn('tanggal',function($item){
                    return Carbon::parse($item->created_at)->format('d/m/Y H:i:s');
                })
                ->addColumn('pasien',function($item){
                    if($item->detail){
                        return count($item->detail);
                    }else{
                        return '0';
                    }
                })
                ->rawColumns(['action','tanggal','pasien'])
                ->make()
                ;

        }

        // dd($transactions);
        return view('pages.hasilswab')->with(array(
                                                        'lab' => $lab,
                                                        'daftarlab' => $daftarlab
                                                    ));
    }

   public function hasilpdf($id,$no_lab)
    {
        $hasil = DetailTransaction::where('transaction_id',$id)->with('pasien')->get();
        $transactions = Transaction::where('id',$id)->first();
        $lab = Lab::where('no_lab',$no_lab)->first();
        $qrcode = base64_encode(QrCode::format('svg')->size(170)->errorCorrection('H')->generate(route('result',$transactions->code)));
        $pdf = PDF::loadView('pdf.hasil',compact('hasil','lab','transactions','qrcode'));
        return $pdf->stream("hasil-".$id.".pdf", array("Attachment" => false));
    }

    public function hasilemail($code)
    {
        $transactions = Transaction::where('code',$code)->first();
        $hasil = DetailTransaction::where('transaction_id',$transactions->id)->with('pasien')->get();

        $qrcode = base64_encode(QrCode::format('svg')->size(170)->errorCorrection('H')->generate(route('result',$transactions->code)));
        $lab = Lab::where('id',$transactions->lab_id)->first();
        $pdf = PDF::loadView('pdf.hasil',compact('hasil','lab','transactions','qrcode'));
        return $pdf->stream("hasil-".$code.".pdf", array("Attachment" => false));
    }

    public function testpdf()
    {
        $hasil = DetailTransaction::where('transaction_id',15)->with('pasien')->get();
        $lab = Lab::where('id', 6)->first();
        return view('pdf.hasil',compact('hasil','lab'));
    }

    public function pindahlab(Request $request)
    {
        $data = Transaction::where('id',$request->idtrans)->first();
        $data->lab_id = $request->lab;
        $data->save();

        return back()->with(['success' => 'Data Berhasil di pindahkan']);
    }
}
