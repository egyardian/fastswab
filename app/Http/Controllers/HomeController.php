<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Pasien;
Use App\DetailTransaction;
Use App\Transaction;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlahantigen = DetailTransaction::where('name_test','Swab Antigen')->whereNotNull('hasil_test')->count();
        $antigenpositif = DetailTransaction::where('name_test','Swab Antigen')->where('hasil_test','like','%POSITIF%')->count();
        $antigennegatif = DetailTransaction::where('name_test','Swab Antigen')->where('hasil_test','like','%NEGATIF%')->count();

        $jumlahantibody = DetailTransaction::where('name_test','Antibody')->whereNotNull('hasil_test')->count();
        $antibodyreaktifigm = DetailTransaction::where('name_test','Antibody')->where('hasil_test','Reaktif IgM')->count();
        $antibodynonreaktifigm = DetailTransaction::where('name_test','Antibody')->where('hasil_test','Non Reaktif IgM')->count();
        $antibodyreaktifigg = DetailTransaction::where('name_test','Antibody')->where('hasil_test','Reaktif IgG')->count();
        $antibodynonreaktifigg = DetailTransaction::where('name_test','Antibody')->where('hasil_test','Non Reaktif IgG')->count();

        $jumlahpcr = DetailTransaction::where('name_test','like','%Swab PCR%')->whereNotNull('hasil_test')->count();
        $pcrpositif = DetailTransaction::where('name_test','like','%Swab PCR%')->where('hasil_test','like','%Positif%')->count();
        $pcrnegatif = DetailTransaction::where('name_test','like','%Swab PCR%')->where('hasil_test','like','%Negatif%')->count();

        $jumlahpcrexpress = DetailTransaction::where('name_test','PCR Express')->whereNotNull('hasil_test')->count();
        $pcrexpresspositif = DetailTransaction::where('name_test','PCR Express')->where('hasil_test','like','%Positif%')->count();
        $pcrexpressnegatif = DetailTransaction::where('name_test','PCR Express')->where('hasil_test','like','%Negatif%')->count();

        $pasien = DetailTransaction::whereNotNull('hasil_test')->count();
        $total = Transaction::where('status',1)->has('detail')->sum('amount');
        $totalhariini = Transaction::where('status',1)->has('detail')->whereDate('created_at', Carbon::today())->sum('amount');
        // dd($total);
        return view('pages.home',compact('jumlahantigen','antigennegatif','antigenpositif',
        'jumlahantibody','antibodyreaktifigm','antibodynonreaktifigm','antibodyreaktifigg','antibodynonreaktifigg',
        'jumlahpcr','pcrnegatif','pcrpositif',
        'jumlahpcrexpress','pcrexpressnegatif','pcrexpresspositif',
        'pasien','total','totalhariini'));
    }
}
