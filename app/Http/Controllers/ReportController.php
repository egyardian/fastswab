<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailTransaction;
use App\Transaction;
use App\Lab;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use PDF;
use Illuminate\Database\Eloquent\Builder;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ReportController extends Controller
{
    public function report(Request $request)
    {
            $range = null;
            $range = $request->range;
            $lab_id = null;
            $lab_id = $request->lab_id;

            if($lab_id){
                if ($range == '1') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->with('pasien','transaction.lab')->whereDate('updated_at', Carbon::today())->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
    $query->where('id',$lab_id);
})->has('transaction.lab')->get();
                }
                elseif ($range == '2') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->with('pasien','transaction.lab')->whereBetween('updated_at', [
                        Carbon::now()->startOfWeek(),
                        Carbon::now()->endOfWeek(),
                    ])->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
    $query->where('id',$lab_id);
})->has('transaction.lab')->get();

                }
                // Range 3 = Last 7 Days
                elseif ($range == '3') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereDate('updated_at', '>', Carbon::now()->subDays(7))->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
    $query->where('id',$lab_id);
})->has('transaction.lab')->get();
                }
                // Range 4 = This Month
                elseif ($range == '4') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereBetween('updated_at', [
                        Carbon::now()->startOfMonth(),
                        Carbon::now()->endOfMonth(),
                    ])->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
    $query->where('id',$lab_id);
})->has('transaction.lab')->get();

                }
                // Range 5 = Last 30 Days
                elseif ($range == '5') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereDate('updated_at', '>', Carbon::now()->subDays(30))->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
    $query->where('id',$lab_id);
})->has('transaction.lab')->get();
                }
                // Range 6 = All time
                elseif ($range == '6') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
    $query->where('id',$lab_id);
})->has('transaction.lab')->get();

                }
                // Range null = From the begining of time
                elseif ($range == null) {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
    $query->where('id',$lab_id);
})->has('transaction.lab')->get();
                }


            }else{
                if ($range == '1') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->with('pasien','transaction.lab')->whereDate('updated_at', Carbon::today())->has('transaction.lab')->get();
                }
                elseif ($range == '2') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->with('pasien','transaction.lab')->whereBetween('updated_at', [
                        Carbon::now()->startOfWeek(),
                        Carbon::now()->endOfWeek(),
                    ])->has('transaction.lab')->get();

                }
                // Range 3 = Last 7 Days
                elseif ($range == '3') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereDate('updated_at', '>', Carbon::now()->subDays(7))->has('transaction.lab')->get();
                }
                // Range 4 = This Month
                elseif ($range == '4') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereBetween('updated_at', [
                        Carbon::now()->startOfMonth(),
                        Carbon::now()->endOfMonth(),
                    ])->has('transaction.lab')->get();

                }
                // Range 5 = Last 30 Days
                elseif ($range == '5') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereDate('updated_at', '>', Carbon::now()->subDays(30))->has('transaction.lab')->get();
                }
                // Range 6 = All time
                elseif ($range == '6') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->has('transaction.lab')->get();

                }
                // Range null = From the begining of time
                elseif ($range == null) {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->has('transaction.lab')->get();
                }
            }
            // dd($transactions);
            // dd($transactions);
            $lab = Lab::all();
        return view('pages.report',compact('transactions','lab'));
    }

    public function printout(Request $request)
    {
            $range = null;
            $range = $request->range;
            $lab_id = null;
            $lab_id = $request->lab_id;

            if($lab_id){
                if ($range == '1') {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->with('pasien','transaction.lab')->whereDate('updated_at', Carbon::today())->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
                        $query->where('id',$lab_id);
                    })->get();

                }
                elseif ($range == '2') {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->with('pasien','transaction.lab')->whereBetween('updated_at', [
                        Carbon::now()->startOfWeek(),
                        Carbon::now()->endOfWeek(),
                    ])->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
                        $query->where('id',$lab_id);
                    })->get();

                }
                // Range 3 = Last 7 Days
                elseif ($range == '3') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereDate('updated_at', '>', Carbon::now()->subDays(7))->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
                        $query->where('id',$lab_id);
                    })->get();
                }
                // Range 4 = This Month
                elseif ($range == '4') {
                    $transactions = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereBetween('updated_at', [
                        Carbon::now()->startOfMonth(),
                        Carbon::now()->endOfMonth(),
                    ])->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
                        $query->where('id',$lab_id);
                    })->get();

                }
                // Range 5 = Last 30 Days
                elseif ($range == '5') {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereDate('updated_at', '>', Carbon::now()->subDays(30))->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
                    $query->where('id',$lab_id);
                })->get();
                }
                // Range 6 = All time
                elseif ($range == '6') {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
                    $query->where('id',$lab_id);
                })->get();

                }
                // Range null = From the begining of time
                elseif ($range == null) {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereHas('transaction.lab', function (Builder $query) use ($lab_id) {
                        $query->where('id',$lab_id);
                    })->get();
                }


            }else{
                if ($range == '1') {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->with('pasien','transaction.lab')->whereDate('updated_at', Carbon::today())->get();
                }
                elseif ($range == '2') {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->with('pasien','transaction.lab')->whereBetween('updated_at', [
                        Carbon::now()->startOfWeek(),
                        Carbon::now()->endOfWeek(),
                    ])->get();

                }
                // Range 3 = Last 7 Days
                elseif ($range == '3') {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereDate('updated_at', '>', Carbon::now()->subDays(7))->get();
                }
                // Range 4 = This Month
                elseif ($range == '4') {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereBetween('updated_at', [
                        Carbon::now()->startOfMonth(),
                        Carbon::now()->endOfMonth(),
                    ])->get();

                }
                // Range 5 = Last 30 Days
                elseif ($range == '5') {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->whereDate('updated_at', '>', Carbon::now()->subDays(30))->get();
                }
                // Range 6 = All time
                elseif ($range == '6') {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->get();

                }
                // Range null = From the begining of time
                elseif ($range == null) {
                    $hasil = DetailTransaction::orderBy('updated_at','desc')->whereNotNull('hasil_test')->with('pasien','transaction.lab')->get();
                }
            }


            // dd($hasil);
            $pdf = PDF::loadView('pdf.hasil2',compact('hasil'));
            return $pdf->stream("hasil-".$range.".pdf", array("Attachment" => false));
    }

    public function hasilsatu($id)
    {
                    $hasil = DetailTransaction::where('id',$id)->with('pasien','transaction.lab')->get();
                    $hasil2 = DetailTransaction::where('id',$id)->first();
                    $transactions = Transaction::where('id',$hasil2->transaction_id)->first();
                    $qrcode = base64_encode(QrCode::format('svg')->size(170)->errorCorrection('H')->generate(route('result',$transactions->code)));
                    $pdf = PDF::loadView('pdf.hasil2',compact('hasil','qrcode'));
                    return $pdf->stream("hasil-".$id.".pdf", array("Attachment" => false));

    }
}
