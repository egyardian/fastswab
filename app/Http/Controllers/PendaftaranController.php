<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lab;
use App\Transaction;
use App\DetailTransaction;
use App\Pasien;
use App\JenisTest;
use Image;
use App\Addon;

class PendaftaranController extends Controller
{
    public function pendaftaran($no_lab)
    {
        $lab = Lab::where('no_lab',$no_lab)->first();
        $jenistests = JenisTest::where('status',1)->get();
        $addons = Addon::where('status',1)->get();
        if($lab){
            return view('pages.pendaftaran')->with(array('lab'=>$lab,'jenistests' => $jenistests,'addons' => $addons,));
        }
        abort(404);
        // dd($lab);
    }

    public function daftar(Request $request)
    {
        $hargaaddon = 0;
        $note="";
        if($request->addon){
            $addon = Addon::whereIn('id',$request->addon)->get();

            foreach($addon as $item){
                $hargaaddon= $hargaaddon+$item->price;
                $note .= $item->name.' - Rp '.number_format($item->price).'<br>';
            }

        }
        $jenistest = JenisTest::findOrFail($request->jenistest_id);
        $transaction = new Transaction;
        $transaction->code = 'FS'.date('YmdHis').rand(100,999);
        $transaction->lab_id = $request->lab_id;
        $transaction->nama = $request->nama[0];
        $transaction->email = $request->email;
        $transaction->phone = $request->phone;
        $transaction->price_test = $jenistest->price_test;
        $transaction->amount = $jenistest->price_test*count($request->nama)+$hargaaddon;
        $transaction->note = $note;

        $transaction->save();

        for ($i=0; $i < count($request->nama) ; $i++) {
            $pasien = new Pasien;
            $pasien->nik = $request->nik[$i];
            $pasien->nama = $request->nama[$i];
            $pasien->jenis_kelamin = $request->jenis_kelamin[$i];
            $pasien->tanggal_lahir = $request->tanggal_lahir[$i];
            $pasien->tempat_lahir = $request->tempat_lahir[$i];
            $pasien->umur = \Carbon\Carbon::parse($request->tanggal_lahir[$i])->diff(\Carbon\Carbon::now())->format('%y tahun, %m bulan');
            $pasien->alamat = $request->alamat[$i];
            if(isset($request->fotoktp[$i])){
                $file = $request->fotoktp[$i];
                $filename = time().rand(0,990).$transaction->id.'.'.$file->getClientOriginalExtension();
                $path = "assets/images/".$filename;
                $fotoktp = Image::make($file);
                $fotoktp->fit(323, 204, function ($constraint) {
                    $constraint->upsize();
                });
                $fotoktp->save($path);
                // $file->move(public_path().'/'.$path, $filename);
                $pasien->fotoktp = $path;
            }

            $pasien->save();

            $detail = new DetailTransaction;
            $detail->transaction_id = $transaction->id;
            $detail->pasien_id = $pasien->id;
            $detail->tanggal_test = date('Y-m-d');
            $detail->suhu_badan = $request->suhu_badan[$i];
            $detail->name_test = $jenistest->name_test;
            $detail->expired = $jenistest->expire_day;
            $detail->save();
        }

        return redirect()->route('pendaftaranqrcode',[$transaction->code]);

    }

    public function pendaftaranqrcode($code)
    {
        return view('pages.pendaftaranqrcode',compact('code'));
    }
}
