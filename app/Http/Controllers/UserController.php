<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Lab;



class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereNotIn('role',[1])->get();
        return view('pages.user')->with(array('users' => $users));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lab = Lab::all();
        return view('pages.usercreate')->with(array('lab' => $lab));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
            'username' => 'required|unique:users,username',
            'password' => 'required|min:6|confirmed',
            'role' => 'required',
            'lab' => 'required',
         ]);
        $user = new User;
        $user->name = $request->name;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->role = $request->role;
        $user->lab_id = $request->lab;
        $user->save();

        return redirect()->route('user.index')->with(['success' => 'Create User Succesfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $lab = Lab::all();
        return view('pages.useredit')->with(array('user' => $user,'lab' => $lab,));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
            'username' => 'required|unique:users,username,'.$id,
            'role' => 'required',
            'lab' => 'required',
         ]);

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->role = $request->role;
        $user->lab_id = $request->lab;
        $user->save();

        return back()->with(['success' => 'Edit User Succesfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $user = User::findOrFail($request->iduser);
        $user->delete();
        return back()->with(['error' => 'Delete User Success']);
    }

    public function changepassword(Request $request, $id)
    {
        $this->validate($request,[
            'password' => 'required|min:6',
         ]);

        $user = User::findOrFail($id);
        $user->password = Hash::make($request->password);
        $user->save();

        return back()->with(['success' => 'Change Password Succesfully']);
    }

}
