<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    function detail(){
        return $this->hasMany(DetailTransaction::class);
    }

    function lab(){
        return $this->belongsTo(Lab::class);
    }
}
