<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailTransaction extends Model
{
    use SoftDeletes;

    function pasien(){
        return $this->belongsTo(Pasien::class,'pasien_id');
    }
    function transaction(){
        return $this->belongsTo(Transaction::class,'transaction_id');
    }
}
