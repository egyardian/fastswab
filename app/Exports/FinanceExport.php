<?php

namespace App\Exports;

use App\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


class FinanceExport implements FromQuery, WithMapping, WithColumnFormatting
{
    use Exportable;

    public function __construct($start,$end,$jenis)
    {
        $this->start = $start;
        $this->end = $end;
        $this->jenis = $jenis;
    }

    public function query()
    {
        $jenis = $this->jenis;

        if($jenis == 'all'){
            return Transaction::query()->whereBetween('updated_at',[$this->start,$this->end])
                                            ->has('detail')
                                            ->where('status',1);
        }else{
            return Transaction::query()->whereBetween('updated_at',[$this->start,$this->end])
                                                ->whereHas('detail', function (Builder $query) use($jenis) {
                                                    $query->where('name_test', $jenis);
                                                })
                                                ->where('status',1);

        }
    }

    public function map($row): array
    {
        return [
            $row->code,
            $row->nama,
            $row->email,
            count($row->detail),
            $row->detail->first()->name_test,
            $row->note,
            Date::dateTimeToExcel($row->updated_at),
            $row->amount
        ];
    }

    public function columnFormats(): array
    {
        return [
            'G' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }
}
